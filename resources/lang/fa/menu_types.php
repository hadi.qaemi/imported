<?php

return [
    'row'   => 'ردیف',
    'title'   => 'عنوان',
    'action'   => 'عملیات',
    'register'   => 'ثبت',
    'delete'   => 'حذف',
    'successfully_added'   => 'رکورد مورد نظر با موفقیت ثبت شد',
    'sure_to_delete'   => 'آیا از حذف این منو اطمینان دارید؟',
    'menu_deleted'   => 'منو مورد نظر حذف گردید',
];