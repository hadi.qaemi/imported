<?php

return [
    'row_id' => 'ردیف',
    'alerts' => 'اطلاعیه‌ها',
    'create_new_alert' => 'اطلاعیه جدید',
    'edit' => 'ویرایش اطلاعیه',
    'delete_alert' => 'حذف اطلاعیه',
    'name' => 'نام',
    'comment' => 'توضیحات',
    'action' => 'عملیات',
    'are_you_sure' => 'آیا از حذف این اطلاعیه اطمینان دارید؟',
];