<?php

return array(
    'Files' => "فایل های پیوست",
    'pdf' => "pdf",
    'xlsx' => "excel",
    'xls' => "excel",
    'docx' => "word",
    'doc' => "word",
);

