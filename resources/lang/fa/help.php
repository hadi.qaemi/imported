<?php

return
[
    'row' => 'ردیف',
    'title' => 'عنوان راهنما',
    'help_id' => 'شناسه راهنما',
    'title_blocks' => 'بلوک',
    'usages' => 'موارد استفاده',
    'see_also' => 'پیوندها (این موارد را نیز ببینید)',
    'add_see_also' => 'اضافه کردن موارد مشابه',
    'usage_count' => 'صفحات',
    'votes' => 'آرا و نظرات',
    'pages' => 'صفحات آمده',
    'permission' => 'دسترسی',
    'select' => 'انتخاب نمایید',
    'operations' => 'عملیات',
    'context' => 'متن',
];

