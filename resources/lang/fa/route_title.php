<?php
return [
    'calendar'=>'تقویم',
    'sessions'=>'جلسات',
    'invitations'=>'دعوتنامه ها',
    'events'=>'رویدادها',
    'task_showdrafts'=>'مشاهده پیش نویس ها',
    'myassignedtasks_list'=>'لیست وظایف',
    'myassignedtasks_priority'=>'اولویت وظایف',
    'myassignedtasks_state'=>'وضعیت وظایف',
    'mytask_list'=>'نمای جدول',
    'mytask_priority'=>'نمای اولویت',
    'mytask_state'=>'مراحل وظایف من ',
    'myassignedtask_packages'=>'ارجاعات من',
    'mytask_package'=>'بسته کاری- وظایف من ',
    'task_taskpackages'=>'تمام وظایف',
    'orgorgans_list'=>'مدیریت سازمان ها',
    'project' =>'ایجاد پروژه ',
    'project_list'=>'لیست پروژه ها',
    'access_index'=>'مدیریت دسترسی ها',
    'proccess_list'=>'لیست فرآیند ها',
    'menulist'=>'مدیریت منو ها'
];