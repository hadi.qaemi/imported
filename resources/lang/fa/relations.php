<?php

return [
    'relations' => 'روابط',
    'create_new_relation' => 'رابطه جدید',
    'row_id' => 'ردیف',
    'name' => 'نام',
    'direct_name' => 'نام حالت مستقیم',
    'inverse_name' => 'نام حالت معکوس',
    'navigation' => 'عنوان دریچه ناوبری',
    'action' => 'عملیات',
    'edit' => 'ویرایش رابطه',
    'delete_relation' => 'حذف رابطه',
    'are_you_sure' => 'آیا از حذف این رابطه اطمینان دارید؟',
];