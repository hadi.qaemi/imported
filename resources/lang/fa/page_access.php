<?php

return [
    'pages' => 'صفحات',
    'title' => 'عنوان',
    'kind' => 'نوع',
    'access' => 'دسترسی',
    'total_access' => 'دسترسی کلی',
    'add_role' => 'افزودن نقش',
    'remove_role' => 'حذف نقش',
    'add_show_permission' => 'اعمال دسترسی نمایش',
    'delete_show_permission' => 'حذف دسترسی نمایش',
    'add_edit_permission' => 'اعمال دسترسی ویرایش',
    'delete_edit_permission' => 'حذف دسترسی ویرایش',
];