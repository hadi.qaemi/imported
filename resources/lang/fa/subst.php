<?php

return [
    'row_id' => 'ردیف',
    'first' => 'عبارت',
    'second' => 'جایگزین',
    'substs' => 'عبارات',
    'create_new_subst' => 'عبارت جدید',
    'edit' => 'ویرایش',
    'delete_subst' => 'حذف',
    'action' => 'عملیات',
    'are_you_sure' => 'آیا از حذف این مورد اطمینان دارید؟',
];