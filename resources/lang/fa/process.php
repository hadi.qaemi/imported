<?php

return [
    'process_entity_list'   => 'لیست موجودیت های فرآیند',
    'id'                    => 'شناسه',
    'create'                => 'ایجاد',
    'status'                => 'وضعیت',
    'current_level'         => 'مرحله فعلی',
    'create_new_relation'   => 'ایجاد رابطه جدید',
    'relation_title'        => 'عنوان رابطه',
    'process_name'          => 'نام فرآیند',
    'select_process'          => 'انتخاب فرآیند',
    'new_process'           => 'فرآیند جدید',
    'submit_and_create_new_process'     => 'تایید و ثبت فرآیند جدید',
    'show_task_draft_info'              => ' نمایش اطلاعات پیش نویس وظیفه',
    'incomplete_form'       => 'فرم ناقص',
    'row_number'            => 'ردیف',
    'no_process_inserted'  => 'تاکنون فرآیندی ثبت نشده است',
    'relation_title'        => 'نام رابطه'
];