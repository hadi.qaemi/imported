@include('layouts.helpers.common.sections.helpers.nav_bar.auth_modals.login_modal')
@include('layouts.helpers.common.sections.helpers.nav_bar.auth_modals.register_modal')
@include('layouts.helpers.common.sections.helpers.nav_bar.auth_modals.remember_password_modal')
@include('layouts.helpers.common.sections.helpers.nav_bar.auth_modals.login_w_message')

{{--JS--}}
@include('layouts.helpers.common.sections.helpers.nav_bar.auth_modals.scripts.login_register_inline_js')