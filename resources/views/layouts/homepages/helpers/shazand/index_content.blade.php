<div id="content_in_center">
    <div class="row">
        <div class="col-xs-6 col-sm-4 col-md-2">
            <div class="inner_content">
                <a href="2">
                    <img src="{{url('theme/shazand/img/thumbnail1.png')}}" alt="دانشنامه">
                    <p class="caption">دانشنامه</p>
                </a>
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-2" >
            <div class="inner_content">
                <a href="3">
                    <img src="{{url('/theme/shazand/img/thumbnail2.png')}}" alt="کتابخانه">
                    <p class="caption">کتابخانه</p>
                </a>
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-2">
            <div class="inner_content">
                <a href="33300">
                    <img src="{{url('theme/shazand/img/thumbnail3.png')}}" alt="پژوهش">
                    <p class="caption">پژوهش</p>
                </a>
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-2">
            <div class="inner_content">
                <a href="33310">
                    <img src="{{url('theme/shazand/img/thumbnail4.png')}}" alt="اتاق فکر">
                    <p class="caption">اتاق فکر</p>
                </a>
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-2">
            <div class="inner_content">
                <a href="6">
                    <img src="{{url('theme/shazand/img/thumbnail5.png')}}" alt="پرس و جو">
                    <p class="caption">پرس و جو</p>
                </a>
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-2">
            <div class="inner_content">
                <a href="4">
                    <img src="{{url('theme/shazand/img/thumbnail6.png')}}" alt="شبکه اجتماعی">
                    <p class="caption">شبکه اجتماعی</p>
                </a>
            </div>
        </div>
    </div>
</div>