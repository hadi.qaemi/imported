<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>درخواست تغییر کلمه عبور - {{ config('constants.SiteTitle') }}</title>


</head>
<body style="direction: rtl; -webkit-text-size-adjust: none; box-sizing: border-box; color: #74787E; font-family: tahoma, 'Helvetica Neue', Helvetica, sans-serif; height: 100%; line-height: 1.4; margin: 0; width: 100% !important;" bgcolor="#F2F4F6"><style type="text/css">
    body {
        width: 100% !important; height: 100%; margin: 0; line-height: 1.4; background-color: #F2F4F6; color: #74787E; -webkit-text-size-adjust: none;
    }
    @media only screen and (max-width: 600px) {
        .email-body_inner {
            width: 100% !important;
        }
        .email-footer {
            width: 100% !important;
        }
    }
    @media only screen and (max-width: 500px) {
        .button {
            width: 100% !important;
        }
    }
</style>
<table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0" style="box-sizing: border-box; font-family: tahoma, 'Helvetica Neue', Helvetica, sans-serif; margin: 0; padding: 0; width: 100%;" bgcolor="#F2F4F6">
    <tr>
        <td align="center" style="box-sizing: border-box; font-family: tahoma, 'Helvetica Neue', Helvetica, sans-serif; word-break: break-word;">
            <table class="email-content" width="100%" cellpadding="0" cellspacing="0" style="box-sizing: border-box; font-family: tahoma, 'Helvetica Neue', Helvetica, sans-serif; margin: 0; padding: 0; width: 100%;">
                <tr>
                    <td class="email-masthead" style="box-sizing: border-box; font-family: tahoma, 'Helvetica Neue', Helvetica, sans-serif; padding: 25px 0; word-break: break-word;" align="center">
                        <a href="https://example.com" class="email-masthead_name" style="box-sizing: border-box; color: #bbbfc3; font-family: tahoma, 'Helvetica Neue', Helvetica, sans-serif; font-size: 16px; font-weight: bold; text-decoration: none; text-shadow: 0 1px 0 white;">
                            {{ config('constants.SiteTitle') }}
                        </a>
                    </td>
                </tr>

                <tr>
                    <td class="email-body" width="100%" cellpadding="0" cellspacing="0" style="-premailer-cellpadding: 0; -premailer-cellspacing: 0; border-bottom-color: #EDEFF2; border-bottom-style: solid; border-bottom-width: 1px; border-top-color: #EDEFF2; border-top-style: solid; border-top-width: 1px; box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0; padding: 0; width: 100%; word-break: break-word;" bgcolor="#FFFFFF">
                        <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0" style="box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0 auto; padding: 0; width: 570px;" bgcolor="#FFFFFF">

                            <tr>
                                <td class="content-cell" style="box-sizing: border-box; font-family: tahoma, 'Helvetica Neue', Helvetica, sans-serif; padding: 35px; word-break: break-word;">

                                    <table class="body-sub" style="border-top-color: #EDEFF2; border-top-style: solid; border-top-width: 1px; box-sizing: border-box; font-family: tahoma, 'Helvetica Neue', Helvetica, sans-serif; margin-top: 25px; padding-top: 25px;">
                                        <tr>
                                            <td style="text-align: right; box-sizing: border-box; font-family: tahoma, 'Helvetica Neue', Helvetica, sans-serif; word-break: break-word;">
                                                کلمه عبور با موفقیت تغییر یافت.
                                                <br>
                                            <td style="box-sizing: border-box; font-family: tahoma, 'Helvetica Neue', Helvetica, sans-serif; word-break: break-word;">
                                                <a href="{{ route('home')}}" class="button button--green" target="_blank" style="-webkit-text-size-adjust: none; background: #22BC66; border-color: #22bc66; border-radius: 3px; border-style: solid; border-width: 10px 18px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); box-sizing: border-box; color: #FFF; display: inline-block; font-family: tahoma, 'Helvetica Neue', Helvetica, sans-serif; text-decoration: none;">صفحه اصلی سامانه</a>
                                            </td>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; word-break: break-word;">
                        <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0" style="box-sizing: border-box; font-family: tahoma, 'Helvetica Neue', Helvetica, sans-serif; margin: 0 auto; padding: 0; text-align: center; width: 570px;">
                            <tr>
                                <td class="content-cell" align="center" style="box-sizing: border-box; font-family: tahoma, 'Helvetica Neue', Helvetica, sans-serif; padding: 35px; word-break: break-word;">
                                    <p class="sub align-center" style="box-sizing: border-box; color: #AEAEAE; font-family: tahoma, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 1.5em; margin-top: 0;" align="center">© 2017 تمام حقوق برای {{ config('constants.SiteTitle') }} محفوظ است.</p>
                                    <p class="sub align-center" style="box-sizing: border-box; color: #AEAEAE; font-family: tahoma, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 1.5em; margin-top: 0;" align="center">
                                        <!--[سامانه دریانوردی]-->
                                        <!--<br />آدرس-->
                                        <!--<br />خیابان-->
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
