<style>
    [id^=pages-list-client] img.zoom
    {
        transition: all 0.75s;
    }
    [id^=pages-list-client] img.zoom:hover
    {
        transform: scale(1.1) rotate(-5deg);
    }
    .more_btn_normal
    {
        background-color: #ffff88;
        color: red;
        cursor: pointer;
        margin: auto auto 25px auto;
        padding: 5px 10px;
        width: 100%;
    }
    .more_btn_disabled
    {
        background-color: lightgrey;
        color: gray;
        cursor: default;
    }
</style>