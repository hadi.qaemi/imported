<div class="col-xs-12">
    <div class="pull-left">
        <button type="button" class="btn btn-primary btn_save_avatar">{{ trans('profile.change_profile_and_save') }}</button>
    </div>
</div>
<script>
    $('#input_file_avatar').trigger( "click" );
</script>