<div class="row">
    <div class="col-xs-6">
        {{trans('subst.first')}}
        <input type="text" name="first" class="form-control" value="{{ $subst->first }}">
    </div>
    <div class="col-xs-6">
        {{trans('subst.second')}}
        <input type="text" name="second" class="form-control" value="{{ $subst->second }}">
    </div>
</div>