<style>
    .form_keyword table,
    .form_keyword table tr,
    .form_keyword table tr td
    {
        border: none;
    }
    .form_keyword label
    {
        margin-top: 10px;
    }
    .form_keyword .relation_types_clone,
    .form_keyword .relation_types_delete
    {
        cursor: pointer;
        margin-right: 5px;
    }
    .form_keyword .relation_types_clone
    {
        margin-top: 4px;
    }
    .form_keyword .relation_types_delete
    {
        color: red;
        font-size: 14px;
        float: left;
        margin-top: 8px;
    }
</style>
