<!--- new Reminder modal-------------->
<div id="form-content">
    <div class="reminder_errorMsg"></div>
    <form id="reminder_form" role="form" class="form-horizontal">
        <div id="form-user-event-reminder">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#step1" data-toggle="tab">{{trans('calendar_events.ce_modal_session_navbar_define')}}</a>
                </li>
                {{--<li>--}}
                    {{--<a class="disable-tab" style="background: #fff !important;padding: 14px;margin-right: 2px;border: none !important;">{{trans('calendar_events.ce_modal_session_navbar_setting')}}</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a class="disable-tab" style="background: #fff !important;padding: 14px;margin-right: 2px;border: none !important;">{{trans('calendar_events.ce_modal_session_navbar_note')}}</a>--}}
                {{--</li>--}}
            </ul>
            @php
                $jdate = preg_split('/ /', Request::input('jdate'));

            @endphp
            <div class="tab-content" style="overflow: auto;height: 50vh;padding-bottom: 10vh;">
                <div id="step1" class="tab-pane fade in active">
                    <div class="col-md-12">
                        <div class="">
                            <div class="col-xs-12 noLeftPadding noRightPadding margin-top-20">
                                <div class="col-xs-1">
                                    <label>
                                        {{--<span class="required">*</span>--}}
                                        {{trans('calendar_events.ce_modal_events_title_field_lable')}}
                                    </label>
                                </div>
                                <div class="col-xs-6">
                                    <input name="title" class="form-control" placeholder="" value="{{isset($source['title']) ? $source['title'] : ''}}">
{{--                                    <input type="hidden" {{isset($form_data['htitle']) ? 'name=event_id value='.$form_data["id"].'' : ''}}>--}}
                                    <input name="event_type" type="hidden" value="{{isset($res['action']) ? $res['action'] : 'reminder'}}">
                                    <input name="action_id" type="hidden" value="{{isset($source['id']) ? $source['id'] : ''}}">
                                </div>
                                <div class="col-xs-5">
                                    <div class="pull-right margin-left-10">
                                        <label class="pull-right" style="height: 33px;line-height: 35px">{{trans('tasks.date')}}</label>
                                        <span class="input-group pull-right clsDatePicker pull-right">
                                            <input type="text" class="form-control DatePicker clsDatePicker pull-right" value="{{isset($jdate[0]) ? $jdate[0] : ''}}" autocomplete="off" id="in_day"  name="in_day" aria-describedby="in_day"/>
                                        </span>
                                    </div>
                                    <div class="pull-right">
                                        <label class="pull-right" style="height: 33px;line-height: 35px">{{trans('tasks.time')}}</label>
                                        <span class="input-group pull-right clsDatePicker pull-right">
                                            <input type="text" class="form-control TimePicker clsDatePicker" style="width: 150px" value="{{isset($jdate[1]) ? $jdate[1] : ''}}" autocomplete="off" id="in_time" name="in_time" aria-describedby="in_time"/>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 noLeftPadding noRightPadding margin-top-20">
                                <div class="col-xs-1">
                                    <label>
                                        {{trans('calendar_events.ce_modal_events_cid_field_lable')}}
                                    </label>
                                </div>
                                <div class="col-xs-5">
                                    <select name="cid[]" class="chosen-rtl cid" multiple data-placeholder="{{trans('calendar_events.ce_modal_events_cid_field_lable')}}">
                                        @if(!empty($form_data['hcid']))
                                            @if(!empty($form_data['hcid']))
                                                @foreach($form_data['hcid'] as $hcid)
                                                    <option selected="selected" value="{{ $hcid->id }}">{{ $hcid->title }}</option>
                                                @endforeach
                                            @endif
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="row col-lg-12 noLeftPadding noRightPadding margin-top-20">
                                <div class="row col-lg-1 line-height-35">
                                    <label class="form-radio-label" >{{trans('calendar_events.ce_modal_reminder_added')}}</label>
                                </div>
                                <div class="row col-lg-9 noLeftPadding noRightPadding">
                                    <span class="pull-right noLeftPadding noRightPadding line-height-35 margin-left-10">
                                        <input name="reminderType" id="reminderType1" type="radio" class="form-radio-input" value="1" checked/>
                                        <label class="form-radio-label" for="reminderType1" >{{trans('calendar_events.ce_modal_reminder_in_hour')}}</label>
                                        <input name="reminderType" id="reminderType2" type="radio" class="form-radio-input" value="2"/>
                                        <label class="form-radio-label" for="reminderType2">{{trans('calendar_events.ce_modal_reminder_in_day')}}</label>
                                    </span>
                                    <span class="add-reminder-row-firstType pull-right line-height-35 noLeftPadding noRightPadding" style="display: inline-flex;">
                                        <label class="pull-right" style="height: 33px;line-height: 35px;white-space:nowrap">{{trans('calendar_events.ce_modal_reminder_in_each')}}</label>
                                        <input id="hour_amount" class="form-control pull-right col-xs-3 margin-left-10" placeholder="{{trans('tasks.amount')}}"/>
                                        <label class="pull-right" style="height: 33px;line-height: 35px;white-space:nowrap">{{trans('calendar_events.ce_modal_before_hour')}}</label>
                                    </span>
                                    <span class="add-reminder-row-secondType pull-right line-height-35 noLeftPadding noRightPadding" style="display: inline-flex;">
                                        <label class="pull-right" style="height: 33px;line-height: 35px;white-space:nowrap">{{trans('calendar_events.ce_modal_reminder_in_each')}}</label>
                                        <input id="day_amount" class="form-control pull-right col-xs-2 margin-left-10" placeholder="{{trans('tasks.amount')}}"/>
                                        <label class="pull-right margin-left-10" style="height: 35px;line-height: 35px;white-space:nowrap;">{{trans('calendar_events.ce_modal_before_day')}}</label>
                                        <label class="pull-right" style="height: 35px;line-height: 35px;white-space:nowrap">{{trans('calendar_events.ce_modal_reminderin_hour')}}</label>
                                        <span class="input-group pull-right clsDatePicker pull-right">
                                            <input type="text" class="form-control TimePicker clsDatePicker" style="width: 150px" value=" " id="day_hour" aria-describedby="term">
                                        </span>
                                    </span>
                                    <span class="pull-right noLeftPadding noRightPadding line-height-35">
                                        <i class="fa fa-plus addReminderTimeBtn pointer"></i>
                                    </span>
                                    <div class="row col-xs-12 noLeftPadding noRightPadding line-height-35">
                                        <div class="row col-xs-12 reminderTimeList noLeftPadding noRightPadding line-height-35 {{isset($form_data['in_day']) ? (count($form_data['in_day'])>1 ? '' : 'hidden') : ''}}">
                                            <div class="row col-xs-12 noLeftPadding noRightPadding line-height-35 margin-top-10 {{isset($form_data['in_day']) ? '' : 'hidden'}}" id="ce_modal_reminder_time_list">{{trans('calendar_events.ce_modal_reminder_time_list')}}:</div>
                                            @if(isset($form_data['in_day']))
                                                @for($i=1;$i<=(count($form_data['in_day'])-1);$i++)
                                                    <div class='row col-xs-12'>
                                                        <span class='pull-right col-lg-1 noLeftPadding noRightPadding'></span>
                                                        <span class='pull-right line-height-35 col-lg-4 noLeftPadding noRightPadding margin-right-10'>
                                                            <label class='pull-right noLeftPadding noRightPadding margin-right-10'>{{trans('calendar_events.ce_modal_reminder_in_time')}}</label>
                                                            <input type='hidden' value='{{$form_data['in_day'][$i]['value']}}' name='in_day[]'/>
                                                            <input type='hidden' value='{{$form_data['firstTyp_term'][$i]['value']}}' name='firstTyp_term[]'/>
                                                            <label class='pull-right pull-right margin-right-10'>{{$form_data['in_day'][$i]['value']}}</label>
                                                            <label class='pull-right pull-right margin-right-10'>{{$form_data['firstTyp_term'][$i]['value']}}</label>
                                                        </span>
                                                        <span class='pull-right col-lg-1 margin-right-10 removeReminderTimeBtn'><i class='fa fa-remove pointer'></i></span>
                                                    </div>
                                                @endfor
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="col-xs-12 noLeftPadding noRightPadding margin-top-20">--}}
                                {{--<div class="col-xs-2">--}}

                                    {{--<label--}}
                                            {{--class="form-check-label">{{trans('calendar_events.ce_allday_label')}}--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-xs-10">--}}
                                    {{--<input name="allDay" type="checkbox"--}}
                                           {{--class="form-check-input" value="1">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                </div>
            </div>
            <input name="event_id" type="hidden" value=""/>
        </div>
    </form>
</div>
        {{-- </div>
        </div>
        </div>
        </div>--}}
        <script>

            $(document).on('click', '.removeReminderTimeBtn', function () {
                $(this).parent().remove();
            });

            $(document).on('click', '.removeReminderTimeBtn', function () {
                $(this).parent().remove();
            });
            $(".DatePicker").persianDatepicker({

                autoClose: true,
                format: 'YYYY-MM-DD',

            });
            // $(".DatePicker").val('');
            $(".TimePicker").persianDatepicker({
                format: "HH:mm",
                timePicker: {
                    //showSeconds: false,
                },
                onlyTimePicker: true
            });
            // $(".TimePicker").val('');
            $.ajax({
                url: '{{ URL::route('auto_complete.get_user_calendar')}}',
                type: 'Post', // Send post dat
                dataType: 'json',
                success: function (s) {
                    var options = '';
                    $('.cid').empty();
                    for (var i = 0; i < s.length; i++) {
                        if (s[i].is_default == 1) {
                            options += '<option  selected=true value="' + s[i].id + '">' + s[i].title + '</option>';
                        }
                        else {
                            options += '<option value="' + s[i].id + '">' + s[i].title + '</option>';
                        }


                    }

                    $('.cid').append(options);
                    $('.cid').select2({
                        dir: "rtl",
                        width: '100%',
                    });
                }
            });
            /*############################################################################################*/
            /*---------------------------------------------------------------------------------------------*/
            /*---------------------------------clone of reminder type2--------------------------------------*/
            /*---------------------------------------------------------------------------------------------*/

            $('.add-reminder-row-secondType2').hide();
            $('input[name="reminderType2"]').change(function () {

                if ($('input[name="reminderType2"]:checked').val() == 1) {
                    $('.add-reminder-row-firstType2').show();
                    $('.add-reminder-row-secondType2').hide();
                } else {
                    $('.add-reminder-row-firstType2').hide();
                    $('.add-reminder-row-secondType2').show();
                }
            });
            /*################################################################################################*/
            /*-----------------------------------------------------------------------------------------------*/
            /*---------------------------------clone of reminder type2--------------------------------------*/
            /*---------------------------------------------------------------------------------------------*/
            $('.add-reminder-row-secondType').hide();
            $('input[name="reminderType"]').change(function () {

                if ($('input[name="reminderType"]:checked').val() == 1) {
                    $('.add-reminder-row-firstType').show();
                    $('.add-reminder-row-secondType').hide();
                } else {
                    $('.add-reminder-row-firstType').hide();
                    $('.add-reminder-row-secondType').show();
                }
            });


        </script>