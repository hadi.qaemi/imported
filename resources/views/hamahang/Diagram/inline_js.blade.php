<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
    (function($){
        $.fn.serializeObject = function(){

            var self = this,
                json = {},
                push_counters = {},
                patterns = {
                    "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                    "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
                    "push":     /^$/,
                    "fixed":    /^\d+$/,
                    "named":    /^[a-zA-Z0-9_]+$/
                };


            this.build = function(base, key, value){
                base[key] = value;
                return base;
            };

            this.push_counter = function(key){
                if(push_counters[key] === undefined){
                    push_counters[key] = 0;
                }
                return push_counters[key]++;
            };

            $.each($(this).serializeArray(), function(){

                // skip invalid keys
                if(!patterns.validate.test(this.name)){
                    return;
                }

                var k,
                    keys = this.name.match(patterns.key),
                    merge = this.value,
                    reverse_key = this.name;

                while((k = keys.pop()) !== undefined){

                    // adjust reverse_key
                    reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

                    // push
                    if(k.match(patterns.push)){
                        merge = self.build([], self.push_counter(reverse_key), merge);
                    }

                    // fixed
                    else if(k.match(patterns.fixed)){
                        merge = self.build([], k, merge);
                    }

                    // named
                    else if(k.match(patterns.named)){
                        merge = self.build({}, k, merge);
                    }
                }

                json = $.extend(true, json, merge);
            });

            return json;
        };
    })(jQuery);
    $('#form_filter_priority #title').on('keyup', function (e) {
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        if (keyCode == 13)  // the enter key code
        {
            filter_tasks_priority();
        }
    });
    $('#form_filter_priority #keywords').on('change', function () {
        filter_tasks_priority();
    });
    function filter_tasks_priority() {
        window.table_diagram.destroy();
        readTable($("#form_filter_priority").serializeObject());
    }

    readTable($("#form_filter_priority").serializeObject());
    function  readTable(send_info) {

        LangJson_DataTables = window.LangJson_DataTables;
        LangJson_DataTables.searchPlaceholder = '{{trans('tasks.search_in_task_title_placeholder')}}';
        LangJson_DataTables.emptyTable = '{{trans('tasks.no_task_found')}}';
        LangJson_DataTables.sLoadingRecords = '<div class="loader preloader"></div>';
        // console.log(LangJson_DataTables);
        window.table_diagram = $('#diagramListTable').DataTable({
            dom: "<'row'<'col-sm-12'tr>> <'row'<'col-sm-4'p><'col-sm-4'l><'col-sm-4'i>>",
            "ajax": {
                "url": "{{ route('hamahang.diagram.fetech_all_diagram') }}",
                "type": "POST",
                "data": send_info
            },
            "bSort": true,
            "aaSorting": [2],
            "bSortable": true,
            "autoWidth": false,
            "searching": false,
            "pageLength": 25,
            // "scrollY": 400,
            "language": LangJson_DataTables,
            "processing": false,
            "pageLength": 25,
            "serverSide": true,
            // "scrollY": 400,
            columns: [
                {
                    "data": "id",
                    "mRender": function (data, type, full) {
                        return full.did;

                    },
                    "width": "10%"
                },
                {
                    "data": "title",
                    "mRender": function (data, type, full) {
                        var id = full.id;
                        // split = full.title.split(' ');
                        // sub_title = '';
                        // $.each(split,function(i,val){
                        //     if(i<=10){
                        //         sub_title = sub_title + ' ' + val;
                        //     }else if(i==11){
                        //         sub_title = sub_title + ' ...';
                        //     }
                        // });

                        var keywords = full.keywords.replace(/&quot;/g,'"');
                        keywords = JSON.parse(keywords);
                        data2 = "";
                        $.each(keywords, function(index) {
                            data2 += '<span class="bottom_keywords one_keyword task_keywords" data-id="'+keywords[index].id+ '" ><i class="fa fa-tag"></i> <span style="color: #6391C5;">'+keywords[index].title+'</span></span>';
                        });

                        return "<a class='cursor-pointer jsPanels white-space" + ( full.assignment_assignment==1 ? 'color_grey' : '' ) + "' href='/modals/diagramOptions?did="+full.id+"' data-toggle='tooltip'  data-html='true' " +
                            "title='" + full.title + (full.desc == null ? '' : "\n" + full.desc) + "'" +
                            ">" + full.title + "</a>" +
                            "<div class='' style='margin: 2px 0px;padding: 5px;'>"+data2+"</div>"
                            ;
                    },
                    "width": "50%"
                },
                {
                    "data": "keywords",
                    "mRender": function (data, type, full) {
                        var users = full.users.replace(/&quot;/g,'"');
                        users = JSON.parse(users);
                        data2 = "";
                        $.each(users, function(index) {
                            data2 += users[index].title;
                        });
                        return "<div class='' style='margin: 2px 0px;padding: 5px;'>"+data2+"</div>";
                    },
                    "width": "30%"
                },
                // {"data": "operation",
                //     "mRender": function (data, type, full) {
                //         return '<a class="jsPanels fa fa-cog pointer margin-right-10" data-toggle="tooltip" title="کپی وظیفه" href="/modals/diagramOptions?did='+full.id+'"></a>';
                //     },
                //     "width": "5%"
                // }

//            , {
//                "data": "id", "width": "8%",
//                "bSearchable": false,
//                "bSortable": false,
//                "mRender": function (data, type, full) {
//                    var id = full.id;
//                    return "<a class='cls3' style='margin: 2px' onclick='f(" + full.id + ")' href=\"#\"><i class='fa fa-edit'></i></a><a style='margin:2px;' class='cls3' \
//                                                                                                                                                                                                                                onclick='del(" + full.id + ")' href=\"#\"><i class='fa fa-trash'></i></a>";
//                }
//            }
            ]
        });

    }
    $(".select2_auto_complete_keywords").select2({
        dir: "rtl",
        width: '100%',
        tags: true,
        minimumInputLength: 2,
        insertTag: function(data, tag){
            tag.text = 'جدید: ' + tag.text;
            data.push(tag);
        },
        ajax: {
            url: "{{route('auto_complete.keywords')}}",
            dataType: "json",
            type: "POST",
            quietMillis: 150,
            data: function (term) {
                return {term: term};
            },
            results: function (data) {
                console.log(data);
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.text,
                            id: item.id
                        }
                    })
                };
            }
        }
    });
    $(".select2_auto_complete_user").select2({
        minimumInputLength: 3,
        dir: "rtl",
        width: "100%",
        tags: false,
        ajax: {
            url: "{{route('auto_complete.users')}}",
            dataType: "json",
            type: "POST",
            quietMillis: 150,
            data: function (term) {
                return {
                    term: term
                };
            },
            results: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.text,
                            id: item.id
                        }
                    })
                };
            }
        }
    });

</script>