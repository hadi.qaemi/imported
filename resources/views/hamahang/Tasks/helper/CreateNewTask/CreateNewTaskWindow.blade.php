<style>
    .HFM_ModalOpenBtn{
        border: none !important;
    }
</style>
<div id="tab" class="row table-bordered" style="border-bottom: none">
    <ul class="nav nav-tabs">
        <li class="active" id="define">
            <a href="#tab_t1" data-toggle="tab">عمومی</a>
        </li>
        <li>
            <a href="#tab_t2" data-toggle="tab">تکرار</a>
            {{--<a href="#" data-toggle="tab">تنظیم</a>--}}
        </li>
        <li class="disabled">
            {{--<a href="#tab_t3" data-toggle="tab">منابع</a>--}}
            <a href="#" data-toggle="tab">منابع</a>
        </li>
        <li>
            <a href="#tab_t4" data-toggle="tab">روابط</a>
        </li>
        <li>
            <a href="#tab_t5" data-toggle="tab">اقدام</a>
        </li>
        <li>
            <a href="#tab_t6" data-toggle="tab">گفتگو</a>
        </li>
        {{--<li>--}}
            {{--<a href="#tab_t7" data-toggle="tab">سابقه</a>--}}
        {{--</li>--}}
        <li style="float: left">
            <h5 id="task_type" style="color: blue"></h5>
        </li>
    </ul>
    @php
        $edit_able = 1;
    @endphp
    <form action="{{ route('hamahang.tasks.save_task') }}" class="" name="create_new_task" id="create_new_task" method="post"
          enctype="multipart/form-data">
        <div class="tab-content new-task-form">
            <div class="tab-pane active tab-view" id="tab_t1">
                <div class="row col-lg-12">
                    <div class="col-lg-1 col-md-3 col-sm-4 col-xs-4"><label class="line-height-35">{{ trans('tasks.title') }}</label></div>
                    <div class="col-lg-8">
                        <div class="row">
                            <input type="text" class="form-control" name="title" id="title" placeholder="{{trans('tasks.title')}}" value="{{isset($task->title) ? $task->title : ''}}"/>
                            <input name="event_type" id="event_type" type="hidden">
                            <input name="startdate" id="startdate" type="hidden">
                            <input name="enddate" id="enddate" type="hidden">
                            <input name="endtime" id="endtime" type="hidden">
                            <input name="starttime" id="starttime" type="hidden">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                        <div class="pull-right" style="height: 30px;line-height: 30px;">
                            <input type="radio" name="type" value="0" id="official" {{isset($task->type) ? ($task->type==0 ? 'checked' : '') : 'checked'}}/>
                            <label for="official">{{ trans('app.official') }}</label>
                            <input type="radio" name="type" value="1" id="unofficial" {{isset($task->type) ? ($task->type==1 ? 'checked' : '') : ''}}/>
                            <label for="unofficial">{{ trans('app.unofficial') }}</label>
                        </div>
                    </div>
                </div>
                <div class="row col-lg-12">
                    <div class="col-lg-1 col-md-3 col-sm-4 col-xs-4"><label class="line-height-35">{{ trans('tasks.description') }}</label></div>
                    <div class="col-lg-11">
                        <div id="for-desc">
                            <div class="row col-lg-12 header">
                                <span class="pointer tab_desc active tab_text" rel="tab_text">{{trans('app.text')}}</span>
                                <span class="pointer tab_desc tab_view" rel="tab_view">{{trans('app.view')}}</span>
                            </div>
                            <div class="row main-desc">
                                <textarea class="form-control row content_tab_text content_tab" name="task_desc" id="desc" value="{{@$sel}}" placeholder="{{ trans('tasks.description') }}" cols="30" rows="4">{{isset($task->desc) ? $task->desc : ''}}</textarea>
                                <div class="content_tab_view content_tab hidden">{{isset($task->desc) ? $task->desc : ''}}</div>
                            </div>
                            <div class="filemanager-buttons-client pull-right bottom-desc">
                                {{--<label for="fileToUpload" class="pointer">--}}
                                <label class="pointer">
                                    {{--<input type="file" class="fileToUpload form-control" style="display: none;" id="fileToUpload"/>--}}
                                    <a class="jsPanels pull-left"
                                       href="{{url('/modals/FetchMyFiles?uid='.auth()->id()).'&act='.enCode('task_image')}}"
                                       title="{{trans('app.add_file')}}" style="color:#999;margin-right: 10px">{{trans('app.add_file')}}</a>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
                                        <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path>
                                    </svg>
                                    {{--<div class="display-inline">{{trans('app.add_file')}}</div>--}}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                {{--<div class="row col-lg-12">--}}
                    {{--<div class="col-lg-2 col-md-3 col-sm-4 col-xs-4">--}}
                        {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noRightPadding noLeftPadding line-height-35">--}}
                            {{--<label class="line-height-35 pull-right">{{ trans('app.attachments') }}</label>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-lg-10 line-height-35 pull-right">--}}
                        {{--<div class="filemanager-buttons-client pull-right">--}}
                            {{--<div class="btn btn-default pull-left HFM_ModalOpenBtn" data-section="{{ enCode('CreateNewTask') }}" data-multi_file="Multi" style="margin-right: 0px;">--}}
                                {{--<i class="glyphicon glyphicon-plus-sign" style="color: skyblue"></i>--}}
                                {{--<span>{{trans('app.add_file')}}</span>--}}
                            {{--</div>--}}
                            {{--<div data-section="{{ enCode(session('page_file')) }}"  class="HFM_RemoveAllFileFSS_SubmitBtn btn btn-default pull-left" style=" color:#555;">--}}
                            {{--<i class="glyphicon glyphicon-remove-sign" style=" color:#FF6600;"></i>--}}
                            {{--<span>{{trans('filemanager.remove_all_attachs')}}</span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="pull-right filemanager-title-client">--}}
                            {{--<h4 class="filemanager-title">{{trans('filemanager.attachs')}}</h4>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="row col-lg-12" style="">
                    <div class="col-lg-1 col-md-3 col-sm-4 col-xs-4"><label class="line-height-35">{{ trans('tasks.responsible') }}</label></div>
                    <div class="col-lg-11">
                        <div class="col-sm-5 row" style="padding: 0px;">
                            <select id="new_task_users_responsible" name="users[]" class="select2_auto_complete_user col-xs-12"
                                    data-placeholder="{{trans('tasks.select_some_persons')}}" multiple>
                                @if($urid)
                                    <option value="{{$urid}}" selected>{{$responsible['Name'].' '.$responsible['Family']}}</option>
                                @endif
                                @if(isset($task->Assignments))
                                    @foreach($task->Assignments as $Assignment)
                                        <option value="{{$Assignment->Employee->id}}" selected>{{$Assignment->Employee->Name.' '.$Assignment->Employee->Family}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <span class=" Chosen-LeftIcon"></span>
                        </div>
                        <div class="col-sm-7 line-height-35" style="padding-right: 5px;">

                            <a href="{!! route('modals.setting_user_view',['id_select'=>'new_task_users_responsible']) !!}" class="jsPanels" title="{{ trans('tasks.selecet_user') }}">
                                <span class="icon icon-afzoodane-fard fonts"></span>
                            </a>
                            <input type="radio" name="assign_type" id="use_type1" class="person_option" value="1" checked/>
                            <label class="person_option" for="use_type1" style="margin-left: 0px;">{{ trans('tasks.collective') }}</label>
                            <input type="radio" name="assign_type" id="use_type2" class="person_option" value="2"/>
                            <label class="person_option" for="use_type2" style="margin-left: 0px;">{{ trans('tasks.individual') }}</label>
                            <input type="radio" name="assign_type" id="use_type3" class="person_option" disabled=""/>
                            <label class="person_option" for="use_type2" style="margin-left: 0px;margin-right: 5px">{{ trans('tasks.one_person') }}</label>

                            {{--<input type="checkbox" name="send_mail" id="send_mail" class="send_message" value="1" checked/>--}}
                            {{--<label class="send_message" for="send_mail" style="margin-left: 0px;">{{ trans('tasks.send-mail') }}</label>--}}
                            {{--<input type="checkbox" name="send_sms" id="send_sms" class="send_message" value="1" disabled="disabled"/>--}}
                            {{--<label class="send_message" for="send_sms" style="margin-left: 0px;">{{ trans('tasks.send-sms') }}</label>--}}
                        </div>
                    </div>
                </div>

                <div class="row col-lg-12">
                    <div class="col-lg-1 col-md-3 col-sm-4 col-xs-4">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noRightPadding noLeftPadding line-height-35">
                            <label class="noRightPadding noLeftPadding">{{trans('tasks.priority')}}</label>
                        </div>
                    </div>
                    <div class="col-lg-11 line-height-35">
                        <div class="pull-right" style="height: 30px;line-height: 30px;">
                            <input type="radio" {{$edit_able == 1 ? ' name=importance id=importance_yes ' : 'disabled'}} value="1" {{isset($task->AbroadPriority->importance) ? $task->AbroadPriority->importance==1 : '' ? 'checked' : 'checked'}}/>
                            <label for="importance_yes">{{ trans('tasks.important') }}</label>
                            <input type="radio" {{$edit_able == 1 ? ' name=importance id=importance_no ' : 'disabled'}} value="0" {{isset($task->AbroadPriority->importance) ? $task->AbroadPriority->importance==0 : '' ? 'checked' : ''}}/>
                            <label for="importance_no">{{ trans('tasks.unimportant')}}</label>
                        </div>
                        <div class="pull-right" style="height: 30px;line-height: 30px;">
                            <input type="radio" {{$edit_able == 1 ? ' name=immediate id=immediate_yes ' : 'disabled'}} value="1" {{isset($task->AbroadPriority->immediate) ? $task->AbroadPriority->immediate==1 : '' ? 'checked' : 'checked'}}/>
                            <label for="immediate_yes" >{{ trans('tasks.immediate') }}</label>
                            <input type="radio" {{$edit_able == 1 ? ' name=immediate id=immediate_no ' : 'disabled'}} value="0"  {{isset($task->AbroadPriority->immediate) ? $task->AbroadPriority->immediate==0 : '' ? 'checked' : ''}}/>
                            <label for="immediate_no">{{ trans('tasks.Non-urgent') }}</label>
                        </div>
                    </div>
                </div>
                <div class="row col-lg-12">
                    <div class="col-lg-1 col-md-3 col-sm-4 col-xs-4">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noRightPadding noLeftPadding line-height-35">
                            <label class="noRightPadding noLeftPadding">{{ trans('tasks.do_respite') }}</label>
                        </div>
                    </div>
                    <div class="col-lg-11 line-height-35">
                        <div class="col-xs-12 noRightPadding noLeftPadding">
                            <span class="pull-right padding-left-20">
                                <input type="radio" {{$edit_able == 1 ? ' name=respite_timing_type id=determination_end_date ' : 'disabled'}} onclick="change_normal_task_timing_type(1)" value="1" {{ isset($task['respite_timing_type']) ? ($task['respite_timing_type']==1 ? 'checked' : '') : '' }}/>
                                <label for="determination_end_date">{{ trans('tasks.determination_end_date') }}</label>
                            </span>
                            <span class="pull-right padding-left-20">
                                <input type="radio" {{$edit_able == 1 ? ' name=respite_timing_type id=determination_doing_duration ' : 'disabled'}} onclick="change_normal_task_timing_type(0)" value="0" {{ isset($task['respite_timing_type']) ? ($task['respite_timing_type']==0 ? 'checked' : '') : '' }}/>
                                <label for="determination_doing_duration">{{ trans('tasks.determination_doing_duration') }}</label>
                            </span>
                            {{--<span class="pull-right padding-left-20">--}}
                            {{--<input type="radio" name="respite_timing_type" id="on-time" onclick="change_normal_task_timing_type(-1)" value="2"/>--}}
                            {{--<label for="on-time" style="margin-left: 0px;">{{ trans('tasks.on-time') }}</label>--}}
                            {{--</span>--}}
                            <span class="pull-right padding-left-20">
                                <input type="radio" {{$edit_able == 1 ? ' name=respite_timing_type id=no-detemine ' : 'disabled'}} onclick="" value="3" {{ isset($task['respite_timing_type']) ? ($task['respite_timing_type']==3 ? 'checked' : '') : '' }}/>
                                <label for="no-detemine">{{ trans('tasks.no-detemine') }}</label>
                            </span>
                            <span id="normal_task_timing" class="pull-right line-height-35" style="display: inline-flex">
                                <input class="form-control border-radius col-xs-1 pull-right" style="width: 55px" {{$edit_able == 1 ? ' name="duration_day" id="duration_day" ' : 'disabled'}} value="{{isset($res['respite']['day']) ? $res['respite']['day'] : ''}}" />
                                <label class="pull-right">روز</label>
                                <input class="form-control border-radius col-xs-1 pull-right" style="width: 55px" {{$edit_able == 1 ? ' name="duration_hour" id="duration_hour" ' : 'disabled'}} value="{{isset($res['respite']['hour']) ? $res['respite']['hour'] : ''}}"/>
                                <label class="pull-right">ساعت</label>
                                <input class="form-control border-radius col-xs-1 pull-right" style="width: 55px" {{$edit_able == 1 ? ' name="duration_min" id="duration_min" ' : 'disabled'}} value="{{isset($res['respite']['hour']) ? $res['respite']['min'] : ''}}"/>
                                <label class="pull-right">دقیقه</label>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row col-lg-12" style="border-top: #ccc solid 1px;margin: 10px 0px;padding-top: 10px">
                    <div class="col-lg-1 col-md-3 col-sm-4 col-xs-4"><label class="line-height-35">{{ trans('app.transcript') }}</label></div>
                    <div class="col-lg-11">
                        <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 row" style="padding: 0px;">
                            <select id="new_task_transcripts" name="transcripts[]" class="select2_auto_complete_transcripts"
                                    data-placeholder="{{trans('tasks.select_some_persons')}}" multiple>
                                @if(!empty($task->Transcripts))
                                    @foreach($task->Transcripts as $transcript)
                                        <option selected="selected" value="{{ $transcript->user->id }}">{{ $transcript->user->Name.' '.$transcript->user->Family }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <span class=" Chosen-LeftIcon"></span>
                        </div>
                        <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12 smaller-90 line-height-35" style="padding-right: 5px;">
                            <a href="{!! route('modals.setting_user_view',['id_select'=>'new_task_transcripts']) !!}" class="jsPanels" title="{{ trans('tasks.selecet_user') }}">
                                <span class="icon icon-afzoodane-fard fonts"></span>
                            </a>
                            <input type="checkbox" name="report_on_cr" id="report-type1" value="1" style="display: none" {{isset($task['report_on_create_point']) ? 'checked' : ''}}/>
                            <label for="report-type1" class="" style="display: none" >{{ trans('tasks.report_on_task_creation') }}</label>
                            <input type="checkbox" name="report_on_co" id="report-type2" value="1" style="display: none" {{isset($task['report_on_completion_point']) ? 'checked' : ''}}/>
                            <label for="report-type2" class="" style="display: none" >{{ trans('tasks.report_on_task_completion') }}</label>
                            {{--<input type="checkbox" name="report_to_manager" id="report-type3" value="1" {{isset($task['report_to_managers']) ? 'checked' : ''}} style="display: none" />--}}
                            {{--<label for="report-type3" class="" style="display: none" >{{ trans('tasks.report_on_responsible') }}</label>--}}
                        </div>
                    </div>
                </div>
                <div class="row col-lg-12">
                    <div class="col-lg-1 col-md-3 col-sm-4 col-xs-4"><label class="line-height-35">{{ trans('tasks.keyword') }}</label></div>
                    <div class="col-lg-11">
                        <select id="new_task_keywords" class="select2_auto_complete_keywords" name="keywords[]"
                                data-placeholder="{{trans('tasks.select_some_keywords')}}"
                                multiple="multiple">
                            @if($kdid)
                                <option value="{{$kdid}}" selected>{{$keyword['title']}}</option>
                            @endif
                            @if(!empty($task->Keywords))
                                @foreach($task->Keywords as $task_keywords)
                                    <option selected="selected" value="{{ isset($task_keywords->keyword->id) ? $task_keywords->keyword->id : '' }}">{{ isset($task_keywords->keyword->title) ? $task_keywords->keyword->title : '' }}</option>
                                @endforeach
                            @endif
                        </select>
                        <span class=" Chosen-LeftIcon"></span>
                    </div>
                </div>
                <div class="row col-lg-12">
                    <div class="col-lg-1"><label class="line-height-35">{{ trans('tasks.page') }}</label></div>
                    <div class="col-lg-11">
                        <select id="new_task_pages" class="select2_auto_complete_page " name="pages[]" data-placeholder="{{trans('tasks.can_select_some_options')}}" multiple="multiple">
                            @if($sid)
                                <option value="{{$sid}}" selected>{{$subject->title}}</option>
                            @endif
                            @if(!empty($task->Subjects))
                                @foreach($task->Subjects as $page)
                                    <option selected="selected" value="{{ $page->id }}">{{ $page->title}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="row-fluid">
                    {!! $HFM_CN_Task['ShowResultArea']['CreateNewTask'] !!}
                </div>

                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="draft" id="draft" value="0"/>
                <input type="hidden" name="first_m" id="first_m" value="0"/>
                <input type="hidden" name="first_u" id="first_u" value="0"/>
                <input type="hidden" name="assigner_id" value="125"/>
                <input type="hidden" name="task_form_action" id="task_form_action" value=""/>
                <input type="hidden" id="save_type" name="save_type" value="0"/>
            {!! $HFM_CN_Task['UploadForm'] !!}

            </div>
            <div class="tab-pane tab-view" id="tab_t2">
                <div class="input-group col-xs-12" style="margin: 0 15px 15px 5px;">
                    <div class="col-xs-1 pull-right">
                        <label class="pull-right line-height-35" >{{trans('tasks.every')}}</label>
                    </div>
                    <div class="col-xs-1">
                        <input type="text" id="task_schedul_num" class="form-control" style="" name="task_schedul_num" value="" disabled>
                    </div>
                    <div class="col-xs-2 noRightPadding noLeftPadding">
                        <select id="task_schedul" name="task_schedul" class="form-control line-height-35 border-radius" disabled>
                            {{--<option value="minute">{{trans('tasks.minute')}}</option>--}}
                            {{--<option value="hour">{{trans('tasks.hour')}}</option>--}}
                            <option value="daily">{{trans('tasks.day')}}</option>
                            <option value="weekly" selected="selected">{{trans('tasks.week')}}</option>
                            <option value="monthly">{{trans('tasks.month')}}</option>
                            <option value="seasonly">{{trans('tasks.season')}}</option>
                            <option value="yearly">{{trans('tasks.year')}}</option>
                        </select>
                    </div>
                </div>
                <div class="input-group col-xs-12 " style="margin: 0 15px 15px 5px;">
                    <div class="col-xs-1 pull-right lbl_repeat_in">
                        <label for="r2" class="line-height-35">{{ trans('tasks.repeat_in') }}</label>
                    </div>
                    <div class="col-xs-11 div-schedul">
                        <div class="minute col-xs-12 hidden">
                        </div>
                        <div class="hour col-xs-12 hidden">
                        </div>
                        <div class="daily col-xs-12 hidden">
                            {{--                            {!! Form::text('daily_value', null, ['class' => 'form-control TimePicker line-height-35']) !!}--}}
                        </div>
                        <div class="weekly row">
                            @for ($i = 0; $i < 7; $i++)
                                <div class="input-group pull-right weekly pull-right" style="margin: 0 0 5px 5px;">
                                    <input id="weekly_value_{{$i}}" class="" style="width: 22px;" name="weekly_value[]" type="checkbox" value="{{$i}}" disabled>
                                    <label style="line-height: 10px;" for="{{ "weekly_value_$i" }}">{{trans('tasks.array_weekly_weekdays.'.$i)}}</label>
                                </div>
                            @endfor
                        </div>
                        <div class="monthly hidden">
                            <div class="input-group pull-right monthly col-xs-12" style="padding: 5px;height: 52px;">
                                @for ($i = 0; $i < 5; $i++)
                                    <div class="input-group pull-right monthly pull-right" style="margin: 0 0 5px 5px;">
                                        <input id="monthly_value_{{$i}}" class="" style="width: 22px;" name="monthly_value[]" type="checkbox" value="{{$i}}" disabled>
                                        <label style="line-height: 10px;" for="{{ "monthly_value_$i" }}">{{trans('tasks.array_monthly_months.'.$i)}}</label>
                                    </div>
                                @endfor
                            </div>
                        </div>
                        <div class="seasonly hidden">
                            <div class="input-group pull-right seasonly col-xs-12" style="padding: 5px;height: 52px;">
                                @for ($i = 0; $i < 4; $i++)
                                    <div class="input-group pull-right seasonly pull-right" style="margin: 0 0 5px 5px;">
                                        <input id="seasonly_value_{{$i}}" class="" style="width: 22px;" name="seasonly_value[]" type="checkbox" value="{{$i}}" disabled>
                                        <label style="line-height: 10px;" for="{{ "seasonly_value_$i" }}">{{trans('tasks.array_seasonly_seasons.'.$i)}}</label>
                                    </div>
                                @endfor
                            </div>
                        </div>
                        <div class="yearly hidden">
                            <div class="input-group pull-right yearly col-xs-12" style="padding: 5px;height: 52px;">
                                @for ($i = 0; $i < 12; $i++)
                                    <div class="input-group pull-right yearly pull-right" style="margin: 0 0 5px 5px;">
                                        <input id="yearly_num_{{$i}}" class="" style="width: 10px;" name="yearly_num[]" type="checkbox" value="{{$i}}" disabled>
                                        <label style="line-height: 10px;" for="{{ "yearly_num_$i" }}">{{trans('tasks.array_yearly_years.'.$i)}}</label>
                                    </div>
                                @endfor
                            </div>
                        </div>
                    </div>
                </div>
                <div class="input-group col-xs-12" style="margin: 0 15px 15px 5px;">
                    <div class="col-xs-1 pull-right">
                        <label for="r2" class="line-height-35">{{ trans('tasks.begin') }}</label>
                    </div>
                    <div class="pull-right" style="margin: 0 0 5px 5px;">
                        <input type="text" class="form-control DatePicker_begin_date pull-right" name="schedul_begin_date" aria-describedby="schedul_begin_date" id="schedul_begin_date" disabled>
                    </div>
                    <div class="pull-right" style="margin: 0 0 5px 5px;">
                        <input type="text" class="form-control TimePicker pull-right" name="schedul_begin_time" id="schedul_begin_time" aria-describedby="schedul_begin_time" disabled>
                    </div>
                </div>
                <div class="input-group col-xs-12" style="margin: 0 15px 15px 5px;">
                    <div class="daily col-xs-1 noLeftPadding line-height-35">
                        <label for="r2" class="line-height-35">{{ trans('tasks.end') }}</label>
                    </div>
                    <div class="daily pull-right noLeftPadding line-height-35" style="margin: 0 0 5px 5px;">
                        <span class="input-group-addon edited-addon" style="padding: 0px; margin: 0px;height: 34px !important;">
                            <input type="radio" name="schedul_end_date" value="schedul_end_date_none" id="schedul_end_date_none" disabled/>
                        </span>
                        <span class="input-group-addon edited-addon" style="padding: 0 5px 0 5px; margin: 0 5px 0 5px;">
                            <label for="schedul_end_date_none">{{ trans('tasks.none') }}</label>
                        </span>
                    {{--</div>--}}
                    {{--<div class="daily col-xs-2" style="margin: 0 0 5px 5px;">--}}
                        <span class="input-group-addon edited-addon" style="padding: 0 5px 0 5px; margin: 0 5px 0 5px;">
                            <input type="radio" name="schedul_end_date" value="schedul_end_date_events" id="schedul_end_date_events" disabled/>
                        </span>
                        <span class="input-group-addon edited-addon" style="padding: 0px; margin: 0px;">
                            <label for="schedul_end_date_events">{{ trans('tasks.after') }}</label>
                        </span>
                        <span class="input-group-addon edited-addon" style="padding: 0 5px 0 5px; margin: 0 5px 0 5px;">
                            <input type="text" id="schedul_end_date_events_" class="form-control" style="width: 40px;" name="schedul_end_num_events" value="" disabled>
                        </span>
                        <span class="input-group-addon edited-addon" style="padding: 0px; margin: 0px;">
                            <label for="schedul_end_date_events_">{{ trans('tasks.repeat') }}</label>
                        </span>
                    {{--</div>--}}
                    {{--<div class="daily col-xs-1" style="margin: 0 0 5px 5px;">--}}
                        <span class="input-group-addon edited-addon" style="padding: 0 5px 0 5px; margin: 0 5px 0 5px;height: 34px">
                            <input type="radio" name="schedul_end_date" value="schedul_end_date_date" id="schedul_end_date_date" checked disabled/>
                        </span>
                        <span class="input-group-addon edited-addon" style="padding: 0 5px 0 5px; margin: 0 5px 0 5px;height: 34px">
                            <label for="schedul_end_date_date">{{ trans('tasks.in-date') }}</label>
                        </span>
                    </div>
                    <div class="daily pull-right noRightPadding" style="margin: 0 0 5px 5px;">
                        <span class="input-group-addon edited-addon noRightPadding" style="padding: 0 5px 0 5px; margin: 0 5px 0 5px;">
                            <input type="text" class="form-control DatePicker_end_date_date" name="schedul_end_date_date" aria-describedby="schedul_end_date_date" id="schedul_end_date_" disabled>
                        </span>
                    </div>
                    <div class="daily pull-right noRightPadding" style="margin: 0 0 5px 5px;">
                        <span class="input-group-addon edited-addon" style="padding: 0 5px 0 5px; margin: 0 5px 0 5px;">
                            <input type="text" class="form-control TimePicker" name="schedul_end_date_time" aria-describedby="schedul_end_date_time" id="schedul_end_date_time" disabled>
                        </span>
                    </div>
                </div>
                {{--<div class="input-group col-xs-12" style="margin: 0 15px 15px 5px;">--}}
                    {{--<div class="pull-right" style="height: 30px;line-height: 30px;">--}}
                        {{--<input type="radio" name="kind" value="1" id="kind_activity"/>--}}
                        {{--<label for="kind_activity">{{ trans('tasks.activity') }}</label>--}}
                        {{--<input type="radio" name="kind" value="0" id="kind_event" checked/>--}}
                        {{--<label for="kind_event">{{ trans('tasks.event')}}</label>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
            <div class="tab-pane tab-view" id="tab_t3">
                <div class="col-xs-12">
                    <div class="col-xs-1">
                        <label class="line-height-35">{{ trans('tasks.resource') }}</label>
                    </div>
                    <div class="col-xs-8">
                        <select id="new_task_resources" name="new_task_resources[]" class="select2_auto_complete_resources col-xs-12"
                                {{--<select id="new_task_users" name="class[]" class="select2_auto_complete_tasks col-xs-12"--}}
                                data-placeholder="{{trans('tasks.select_some_options')}}">
                            <option value=""></option>
                        </select>
                        <span style=" position: absolute; left: 20px; top: 10px;" class=""></span>
                    </div>
                    <div class="col-xs-1">
                        <input type="text" id="new_task_resources_amount" class="form-control noRightPadding noLeftPadding text-center" placeholder="{{ trans('tasks.amount') }}"/>
                    </div>
                    <div class="col-xs-1">
                        <input type="text" id="new_task_resources_cost" class="form-control noRightPadding noLeftPadding text-center" placeholder="{{ trans('tasks.cost') }}"/>
                    </div>
                    <div class="col-xs-1">
                        <span class="btn btn-primary fa fa-plus" id="add_resource_task"></span>
                    </div>
                </div>
                <div class="col-xs-12">
                    <table id="ChildsGrid" class="table table-striped table-bordered dt-responsive nowrap display" style="text-align: center" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            {{--<th class="col-xs-1">{{ trans('tasks.number') }}</th>--}}
                            <th class="col-xs-5">{{ trans('tasks.resource') }}</th>
                            <th class="col-xs-1">{{ trans('tasks.amount') }}</th>
                            <th class="col-xs-2">{{ trans('tasks.cost') }}</th>
                            <th class="col-xs-1">{{ trans('tasks.action') }}</th>
                        </tr>
                        </thead>
                        <tbody id="resources_task_list">

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane tab-view" id="tab_t4">
                {{--<div class="row col-lg-12 border-bottom">--}}
                {{--<div class="col-lg-1"><label>{{ trans('tasks.task') }}</label></div>--}}
                {{--<div class="col-lg-11">--}}
                {{--<span class="task_title">{{$task['title']}}</span>--}}
                {{--</div>--}}
                {{--</div>--}}
                <div class="col-xs-12 border-bottom">
                    <span id="task_project">
                        <div class="row col-lg-12 noLeftPadding noRightPadding">
                            <div class="row col-lg-12 noLeftPadding noRightPadding">
                                <div class="col-lg-1"><label class="line-height-35">{{ trans('tasks.project') }}</label></div>
                                <div class="col-lg-10">
                                    <select id="new_task_projects" name="project_tasks[]" class="select2_auto_complete_projects col-xs-12"
                                            data-placeholder="{{trans('tasks.enter_project_name')}}">
                                        <option value=""></option>
                                        {{--@if(isset($project))--}}
                                        {{--<option value="{{$project->id}}" selected>{{$project->title}}</option>--}}
                                        {{--@endif--}}
                                    </select>
                                </div>
                                <div class="col-xs-1">
                                    <span class="fa fa-plus pointer line-height-30" id="add_rel_project"></span>
                                </div>
                            </div>
                            <div class="col-xs-12 border-bottom padding-bottom-10">
                                <table id="ChildsGrid" class="table dt-responsive nowrap display {{isset($task->Projects) ? (count($task->Projects)==0 ? 'hidden' : '') : ''}} ChildsGridProjectRel" style="margin-bottom: 0px;text-align: center" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        {{--<th class="col-xs-1">{{ trans('tasks.number') }}</th>--}}
                                        {{--                                            <th class="col-xs-5">{{ trans('tasks.relation') }}</th>--}}
                                        <th class="col-xs-5 text-right">{{ trans('tasks.project') }}</th>
                                        <th class="col-xs-1 text-right">{{ trans('tasks.weight') }}</th>
                                        <th class="col-xs-1 text-right">{{ trans('tasks.action') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody id="rel_project_list">
                                        @if(isset($task->Projects))
                                            @foreach($task->Projects as $k => $project)
                                                <tr id="num_add_rel_task{{$k+1}}">
                                                    <td class="col-xs-5 text-right">پایین دستی</td>
                                                    <td class="col-xs-5 text-right">
                                                        {{--<pre>--}}
                                                        {{--{{print_r($project)}}--}}
                                                        {{--</pre>--}}
                                                        {{$project->title}}
                                                        <input name="new_task_projects_[]" type="hidden" value="{{$project->id}}"/>
                                                        <input name="new_task_projects_t[]" type="hidden" value="{{$project->title}}"/>
                                                    </td>
                                                    <td class="col-xs-1">
                                                        <input name="new_project_weight[]" class="form-control" type="text" value="{{isset($project) ? $project->weight : ''}}"/>
                                                    </td>
                                                    <td class="col-xs-1">
                                                        <span class="fa fa-trash remove_new_task pointer line-height-35" onclick="remove_new_task({{$k+1}})" for="r2"></span>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row col-lg-12 noLeftPadding noRightPadding margin-top-10">
                            <div class="row col-lg-12 noLeftPadding noRightPadding">
                                <div class="col-xs-1">
                                    <label class="line-height-35">{{ trans('tasks.task') }}</label>
                                </div>
                                <div class="col-xs-10">
                                    <select id="new_task_rel" name="rel_tasks[]" class="select2_auto_complete_tasks col-xs-12"
                                            {{--<select id="new_task_users" name="class[]" class="select2_auto_complete_tasks col-xs-12"--}}
                                            data-placeholder="{{trans('tasks.select_some_options')}}">
                                        <option value=""></option>
                                    </select>
                                    <span style=" position: absolute; left: 20px; top: 10px;" class=""></span>
                                </div>
                                <div class="col-xs-1">
                                    <span class="fa fa-plus pointer line-height-30" id="add_rel_task"></span>
                                </div>
                            </div>
                        </div>
                    </span>
                </div>
                @php
                    $allTasks = isset($task->Tasks1) ? $task->Tasks1->merge($task->Tasks2) : '';
                @endphp
                <div class="col-xs-12">
                    <div id="ChildsGrid" class="table dt-responsive nowrap display ChildsGridtaskRel {{count($allTasks)==0 ? 'hidden' : ''}}" style="text-align: center;margin-top: 20px" cellspacing="0" width="100%">
                        <div class="col-xs-12 noLeftPadding noRightPadding">
                            {{--<th class="col-xs-1">{{ trans('tasks.number') }}</th>--}}
                            <div class="col-xs-5 text-right"><label class="pull-right line-height-30">{{ trans('tasks.relation') }}</label></div>
                            <div class="col-xs-5 text-right"><label class="pull-right line-height-30">{{ trans('tasks.task') }}</label></div>
                            <div class="col-xs-1 text-center"><label class="line-height-30">{{ trans('tasks.weight') }}</label></div>
                            <div class="col-xs-1 text-center"><label class="line-height-30">{{ trans('tasks.action') }}</label></div>
                        </div>
                        <div id="rel_task_list">
                            @if(isset($allTasks) && is_array($allTasks))
                                @foreach($allTasks as $k => $A_task)
                                    <div id="num_add_rel_task{{$k+20}}">
                                        <div class="col-xs-3 text-right line-height-35">
                                            {{$A_task->Task1->title}}
                                            <input name="new_task_tasks_t1[]" type="hidden" value="{{$A_task->Task1->id}}"/>
                                        </div>
                                        <div class="col-xs-4 text-right line-height-35">
                                            <select name="new_task_relation_old[]" class="new_task_relation form-control pull-right noLeftPadding noRightPadding" onchange="new_task_relation(this,{{$k+20}})" style="width: 120px;">
                                                <option value="end_start" {{$A_task->relation != 'end_start' ? '' : 'selected="selected"'}}>{{trans('tasks.end_start')}}</option>
                                                <option value="start_start" {{$A_task->relation != 'start_start' ? '' : 'selected="selected"'}}>{{trans('tasks.start_start')}}</option>
                                                <option value="start_end" {{$A_task->relation != 'start_end' ? '' : 'selected="selected"'}}>{{trans('tasks.start_end')}}</option>
                                                <option value="end_end" {{$A_task->relation != 'end_end' ? '' : 'selected="selected"'}}>{{trans('tasks.end_end')}}</option>
                                                <option value="up" {{$A_task->relation != 'up' ? '' : 'selected="selected"'}}>{{trans('tasks.up')}}</option>
                                                <option value="down" {{$A_task->relation != 'down' ? '' : 'selected="selected"'}}>{{trans('tasks.down')}}</option>
                                                <option value="after" {{$A_task->relation != 'after' ? '' : 'selected="selected"'}}>{{trans('tasks.after')}}</option>
                                                <option value="previous" {{$A_task->relation != 'previous' ? '' : 'selected="selected"'}}>{{trans('tasks.previous')}}</option>
                                            </select>
                                            <label class="input-group pull-right intrupt_div" style="width: 80px;">
                                                <input name="new_task_delay_num_old[]" value="{{$A_task->delay}}" type="text" class="form-control" placeholder="{{trans('tasks.delay')}}"/>
                                            </label>
                                            <label class="input-group pull-right intrupt_div" style="width: 80px;">
                                                <select name="new_task_delay_type_old[]" class="form-control" >
                                                    <option value="day" {{$A_task->delay_type != 'day' ? '' : 'selected="selected"'}}>{{trans('tasks.day')}}</option>
                                                    <option value="week" {{$A_task->delay_type != 'week' ? '' : 'selected="selected"'}}>{{trans('tasks.week')}}</option>
                                                    <option value="month" {{$A_task->delay_type != 'month' ? '' : 'selected="selected"'}}>{{trans('tasks.month')}}</option>
                                                </select>
                                            </label>
                                        </div>
                                        <div class="col-xs-3 line-height-35 text-right">
                                            {{$A_task->Task2->title}}
                                            <input name="new_task_tasks_t2[]" type="hidden" value="{{$A_task->Task2->id}}"/>
                                        </div>
                                        <div class="col-xs-1">
                                            <input name="new_task_weight_old[]" class="form-control" type="text" value="{{$A_task->weight}}"/>
                                        </div>
                                        <div class="col-xs-1">
                                            <span class="fa fa-trash remove_new_task pointer line-height-35" onclick="remove_new_task({{$k+20}})" for="r2"></span>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane tab-view" id="tab_t5">
                <div class="row col-lg-12" style="margin-top: 20px;">
                    <div class="col-lg-1 col-md-3 col-sm-4 col-xs-4 noRightPadding noLeftPadding">
                        <label for="r2" style="height: 30px;line-height: 30px;">{{ trans('tasks.status') }}</label>
                    </div>
                    <div class="col-lg-11 noRightPadding noLeftPadding">
                        <div class="pull-right noRightPadding noLeftPadding" style="height: 30px;line-height: 30px;">
                            <input type="radio" name="task_status" id="not_start" value="0"/>
                            <label for="not_start">{{ trans('tasks.not_start') }}</label>
                        </div>
                        <div class="pull-right" style="height: 30px;line-height: 30px;margin-right: 10px">
                            <input type="radio" name="task_status" id="on_done" value="1"/>
                            <label for="on_done">{{ trans('tasks.on_done')}}</label>
                            <input type="text" id="num_event" class="form-control border-radius" placeholder="{{ trans('tasks.precent_progress') }}" style="width: 40px;display: inline" name="progress" >

                        </div>
                        <div class="pull-right" style="height: 30px;line-height: 30px;margin-right: 10px">
                            <input type="radio" name="task_status" id="status_done" value="2"/>
                            <label for="status_done">{{ trans('tasks.status_done') }}</label>
                        </div>
                        <div class="pull-right" style="height: 30px;line-height: 30px;margin-right: 10px">
                            <input type="radio" name="task_status" id="status_finished" value="3"/>
                            <label for="status_finished">{{ trans('tasks.status_finished') }}</label>
                        </div>
                        <div class="pull-right" style="height: 30px;line-height: 30px;margin-right: 10px">
                            <input type="radio" name="task_status" id="status_suspended" value="4"/>
                            <label for="status_suspended">{{ trans('tasks.status_suspended') }}</label>
                        </div>
                    </div>
                </div>
                <div class="row col-lg-12 margin-top-15">
                    <div class="col-lg-1 col-md-3 col-sm-4 col-xs-4 noRightPadding noLeftPadding">
                        <label class="line-height-35">{{ trans('tasks.description') }}</label>
                    </div>
                    <div class="col-lg-10 noRightPadding noLeftPadding">
                        <div id="for-desc">
                            <div class="row col-lg-12 header">
                                <span class="pointer tab_desc active tab_text" rel="tab_text">{{trans('app.text')}}</span>
                                <span class="pointer tab_desc tab_view" rel="tab_view">{{trans('app.view')}}</span>
                            </div>
                            <div class="row main-desc">
                                <textarea class="form-control row content_tab_text content_tab" name="action_explain" id="explain" placeholder="{{ trans('tasks.description') }}" cols="30" rows="4"></textarea>
                                <div class="content_tab_view content_tab hidden">
                                </div>
                            </div>
                            <div class="filemanager-buttons-client pull-right bottom-desc">
                                <label for="fileToUpload2" class="pointer">
                                    <input type="file" class="fileToUpload2 form-control" style="display: none;" id="fileToUpload2"/>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
                                        <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path>
                                    </svg>
                                    <div class="display-inline">{{trans('app.add_file')}}</div>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row col-lg-12 margin-top-20">
                    <div class="col-lg-1 col-md-3 col-sm-4 col-xs-4 noRightPadding noLeftPadding">
                        <label for="r2" style="height: 30px;line-height: 30px;">{{ trans('tasks.report') }}</label>
                    </div>
                    <div class="pull-right height-30 line-height-30">
                        <label for="determined-time">{{ trans('tasks.set_action_on') }}: </label>
                        <input type="text" class="form-control border-radius DatePicker" style="display: inline;width: 110px;" id="action_date" name="action_date" aria-describedby="respite_date">
                    </div>
                    <div class="pull-right height-30 line-height-30 margin-right-10">
                        <label for="determined-time">{{ trans('tasks.duration') }}</label>
                    </div>
                    <div class="pull-right height-30 line-height-30 margin-right-10">
                        <input type="number" class="form-control border-radius" style="display: inline;width: 50px;" id="action_duration_act" name="action_duration_act" placeholder="{{ trans('tasks.duration') }}" aria-describedby="respite_date">
                    </div>
                    <div class="pull-right height-30 line-height-30 margin-right-10">
                        <select class="form-control" id="action_duration_act_type">
                            <option value="ساعت">ساعت</option>
                            <option value="دقیقه">دقیقه</option>
                        </select>
                    </div>
                    <div class="pull-right height-30 line-height-30 margin-right-10">
                        <label for="determined-time">{{ trans('tasks.from').' '.trans('tasks.hour') }}</label>
                        <input type="text" class="form-control border-radius TimePicker" value="0" style="display: inline" id="action_time_from" name="action_time_from" aria-describedby="respite_time">
                        <label for="determined-time">{{ trans('tasks.to').' '.trans('tasks.hour') }}</label>
                        <input type="text" class="form-control border-radius TimePicker" value="" style="display: inline" id="action_time_to" name="action_time_to" aria-describedby="respite_time">
                    </div>
                    <div class="pull-right height-30 line-height-30 margin-right-10">
                        <i class="btn btn-primary fa fa-plus" id="add_btn_action"></i>
                    </div>
                </div>
                <div class="row col-lg-12 margin-top-15">
                    <div class="col-lg-1 col-md-3 col-sm-4 col-xs-4 noRightPadding noLeftPadding">
                        <label class="line-height-35">مواجه</label>
                    </div>
                    <div class="col-lg-11 col-md-9 col-sm-8 col-xs-8 noRightPadding noLeftPadding">
                        <div class="pull-right">
                            <input type="radio" name="reject_assigner" id="reject_assigner" class="reject_assigner" value="3" checked/>
                            <label for="reject_assigner" style="height: 30px;line-height: 30px;" class="rejected_options noRightPadding noLeftPadding">{{ trans('tasks.accept') }}</label>
                            <input type="radio" name="reject_assigner" id="reject_assigner0" class="" value="1" style="display: inline" disabled/>
                            <label for="reject_assigner0" style="height: 30px;line-height: 30px;" class="rejected_options noRightPadding noLeftPadding">{{ trans('tasks.reject') }}</label>
                            <input type="radio" name="reject_assigner" id="reject_assigner1" class="" value="0" style="display: inline"/>
                            <label for="reject_assigner1" class="rejected_options" >{{ trans('tasks.reject_to') }}</label>
                        </div>
                        <div class="pull-right width-400">
                            <select id="assigns_new" name="assigns_new[]" class="select2_auto_complete_transcripts assingned_options"
                                    data-placeholder="{{trans('tasks.select_some_options')}}" multiple disabled=""></select>
                            <span class=" Chosen-LeftIcon"></span>
                        </div>
                        <div class="pull-right margin-right-10 line-height-35">
                            <a href="{!! route('modals.setting_user_view',['id_select'=>'assigns_new']) !!}" class="jsPanels assingned_options" title="{{ trans('tasks.selecet_user') }}">
                                <span class="icon icon-afzoodane-fard fonts"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row col-lg-12 margin-top-15 margin-bottom-20 border-bottom padding-bottom-20">
                    <div class="col-lg-1 col-md-3 col-sm-4 col-xs-4 noRightPadding noLeftPadding">
                        <label class="line-height-35">{{ trans('tasks.description') }}</label>
                    </div>
                    <div class="col-lg-10">
                        <input type="text" class="rejected_options form-control border-radius" placeholder="توضیح ..." name="explain_reject" id="explain_reject" value=""/>
                    </div>
                </div>

                <div class="row col-lg-12 noLeftPadding noRightPadding">
                    <div class="col-xs-1">
                        <input type="checkbox" class="" name="end_on_assigner_accept" id="end_on_assigner_accept" style="height: 20px"/>
                    </div>
                    <div class="col-xs-10">
                        <label for="end_on_assigner_accept">{{ trans('tasks.modal_task_details_assignor_accept_or_ended') }}</label>
                    </div>
                </div>
                <div class="row col-lg-12 noLeftPadding noRightPadding">
                    <div class="col-xs-1">
                        <input type="checkbox" class="" name="transferable" id="transferable" style="height: 20px" checked/>
                    </div>
                    <div class="col-xs-11">
                        <label for="transferable">{{ trans('tasks.modal_task_details_assignor_to_another') }}</label>
                    </div>
                </div>
            </div>
            <div class="tab-pane tab-view" id="tab_t6">
                <div class="col-xs-12 margin-bottom-30">
                    <div class="col-xs-11">
                        <input type="text" id="message" class="form-control border-radius" placeholder="پیام"/>
                        <input type="hidden" id="user" class="form-control border-radius" value="{{Session::get('Name').' '.Session::get('Family')}}"/>
                        <input type="hidden" id="user_id" class="form-control border-radius" value="{{Session::get('uid')}}"/>
                    </div>
                    <div class="col-xs-1 pointer line-height-35">
                        <a id="add_message_task_transcription" class="btn btn-primary" >{{trans('app.submit')}}</a>
                    </div>
                </div>
                <div class="col-xs-12">
                    <table id="ChildsGrid" class="table table-bordered dt-responsive nowrap display" style="text-align: center" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th class="col-xs-2">کاربر</th>
                            <th class="col-xs-2">زمان</th>
                            <th class="col-xs-8">پیام</th>
                        </tr>
                        </thead>
                        <tbody id="message_task_list"></tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane tab-view" id="tab_t7">
                <div class="col-xs-12">
                    <table id="ChildsGrid" class="table table-striped table-bordered dt-responsive nowrap display" style="text-align: center" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            {{--<th class="col-xs-1">{{ trans('tasks.number') }}</th>--}}
                            <th class="col-xs-2">{{ trans('tasks.user') }}</th>
                            <th class="col-xs-5">{{ trans('tasks.action') }}</th>
                            <th class="col-xs-5">{{ trans('tasks.time') }}</th>
                        </tr>
                        </thead>
                        <tbody id="message_task_list">

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </form>
</div>

{{--<div class="col-xs-12 col-md-12">--}}
    {{--<form action="{{ route('hamahang.tasks.save_task') }}" class="" name="create_new_task" id="create_new_task" method="post"--}}
          {{--enctype="multipart/form-data">--}}
        {{--<table class="table col-xs-12">--}}
            {{--<tr>--}}
                {{--<td class="width-120">--}}
                    {{--<label class="line-height-35">{{ trans('tasks.title') }}</label>--}}
                {{--</td>--}}
                {{--<td>--}}
                    {{--<div class="row-fluid">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-sm-7">--}}
                                {{--<input type="text" class="form-control" name="title" id="title"/>--}}
                            {{--</div>--}}
                            {{--<div class="clearfixed"></div>--}}
                        {{--</div>--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-xs-12 line-height-35">--}}
                                {{--<div class="pull-right" style="margin: 0 0 0 10px;">--}}
                                    {{--<input type="radio" name="type" value="0" checked/>--}}
                                    {{--<label for="r1">{{ trans('app.official') }}</label>--}}
                                    {{--<input type="radio" name="type" value="1"/>--}}
                                    {{--<label for="r2">{{ trans('app.unofficial') }}</label>--}}
                                {{--</div>--}}
                                {{--<div class="pull-right" style="margin: 0 10px;">--}}
                                    {{--<input type="radio" name="importance" value="1"/>--}}
                                    {{--<label>{{ trans('tasks.important') }}</label>--}}
                                    {{--<input type="radio" name="importance" value="0" checked/>--}}
                                    {{--<label>{{ trans('tasks.unimportant')}}</label>--}}
                                {{--</div>--}}

                                {{--<div class="pull-right" style="margin: 0 10px;">--}}
                                    {{--<input type="radio" name="immediate" value="1"/>--}}
                                    {{--<label>{{ trans('tasks.immediate') }}</label>--}}
                                    {{--<input type="radio" name="immediate" value="0" checked/>--}}
                                    {{--<label>{{ trans('tasks.Non-urgent') }}</label>--}}
                                {{--</div>--}}
                                {{--<div class="clearfix"></div>--}}
                            {{--</div>--}}
                            {{--<div class="clearfix"></div>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                {{--</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
                {{--<td class="width-120">--}}
                    {{--<label class="line-height-35">{{ trans('tasks.importance') }}--}}
                        {{--{{ trans('tasks.immediacy') }}--}}
                    {{--</label>--}}
                {{--</td>--}}
                {{--<td>--}}
                    {{--<div class="row-fluid">--}}
                        {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-inline line-height-35">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-xs-6">--}}
                                    {{--<label>{{ trans('tasks.importance') }} :</label>--}}
                                    {{--<span class="input-group">--}}
                                        {{--<input type="radio" name="importance" value="1"/>--}}
                                        {{--<label>{{ trans('tasks.important') }}</label>--}}
                                        {{--<input type="radio" name="importance" value="0" checked/>--}}
                                        {{--<label>{{ trans('tasks.unimportant')}}</label>--}}
                                    {{--</span>--}}
                                {{--</div>--}}
                                {{--<div class="col-xs-6">--}}
                                    {{--<label>{{ trans('tasks.immediacy') }} :</label>--}}
                                    {{--<span class="input-group">--}}
                                        {{--<input type="radio" name="immediate" value="1"/>--}}
                                        {{--<label>{{ trans('tasks.immediate') }}</label>--}}
                                        {{--<input type="radio" name="immediate" value="0" checked/>--}}
                                        {{--<label>{{ trans('tasks.Non-urgent') }}</label>--}}
                                    {{--</span>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                {{--</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
                {{--<td>{{ trans('tasks.task_type') }}</td>--}}
                {{--<td>--}}
                    {{--<div class="row">--}}
                        {{--<table class="table no-margin no-padding">--}}
                            {{--<tr>--}}
                                {{--<td style="border-top: none;">--}}
                                    {{--<input type="radio" id="use_type0" name="use_type" value="0" onclick="change_respite_type(0)" checked/>  <!-------- normal task  ---->--}}
                                    {{--<label for="r1">{{ trans('tasks.ordinary_task') }}</label>--}}
                                    {{--<input type="radio" id="use_type1" name="use_type" value="1" onclick="change_respite_type(1)"/>          <!-------- project task  ---->--}}
                                    {{--<label for="r2">{{ trans('tasks.project_task') }}</label>--}}
                                    {{--<input type="radio" id="use_type2" name="use_type" value="2" onclick="change_respite_type(2)"/>          <!-------- process task  ---->--}}
                                    {{--<label for="r2">{{ trans('tasks.process_task') }}</label>--}}
                                {{--</td>--}}
                                {{--<td style="border-top: none;">--}}
                                    {{--<div id="project_span" class="pull-right"></div>--}}
                                {{--</td>--}}
                            {{--</tr>--}}
                        {{--</table>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                {{--</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
                {{--<td class="width-120">--}}
                    {{--<label class="line-height-35">{{ trans('tasks.do_respite') }}</label>--}}
                {{--</td>--}}
                {{--<td>--}}
                    {{--<div id="respite_span">--}}
                        {{--<table class="table col-xs-12 no-padding no-margin">--}}
                            {{--<tr>--}}
                                {{--<td style="border-top: none;">--}}
                                    {{--<input type="radio" name="respite_timing_type" onclick="change_normal_task_timing_type(0)" value="0" checked/>--}}
                                    {{--<label for="r2">{{ trans('tasks.determination_doing_duration') }}</label>--}}
                                    {{--<input type="radio" name="respite_timing_type" onclick="change_normal_task_timing_type(1)" value="1"/>--}}
                                    {{--<label for="r1">{{ trans('tasks.determination_end_date') }}</label>--}}
                                {{--</td>--}}
                                {{--<td style="border-top: none;">--}}
                                    {{--<div id="normal_task_timing">--}}
                                        {{--<div class="row-fluid">--}}
                                            {{--<div class=" col-md-12 col-sm-12 col-xs-12 form-inline line-height-35">--}}
                                                {{--<div class="row-fluid">--}}
                                                    {{--<div class="col-sm-12 col-xs-12 form-inline">--}}
                                                        {{--<input class="form-control col-xs-1 pull-right" style="width: 55px" name="duration_day" id="duration_day" value="1"/>--}}
                                                        {{--<label class="pull-right">روز</label>--}}
                                                        {{--<input class="form-control col-xs-1 pull-right" style="width: 55px" name="duration_hour" id="duration_hour" value="0"/>--}}
                                                        {{--<label class="pull-right">ساعت</label>--}}
                                                        {{--<input class="form-control col-xs-1 pull-right" style="width: 55px" name="duration_min" id="duration_min" value="0"/>--}}
                                                        {{--<label class="pull-right">دقیقه</label>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<div class="clearfix"></div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</td>--}}
                            {{--</tr>--}}
                        {{--</table>--}}
                    {{--</div>--}}
                {{--</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
                {{--<td class="width-120">--}}
                    {{--<label class="line-height-35">{{ trans('tasks.responsible') }}</label>--}}
                {{--</td>--}}
                {{--<td>--}}
                    {{--<div class="row-fluid">--}}
                        {{--<div class="col-sm-7 row" style="padding-left: 0px;">--}}
                            {{--<select id="new_task_users" name="users[]" class="select2_auto_complete_user col-xs-12"--}}
                                    {{--data-placeholder="{{trans('tasks.select_some_options')}}" multiple>--}}
                                {{--<option value=""></option>--}}
                            {{--</select>--}}
                            {{--<span style=" position: absolute; left: 20px; top: 10px;" class=""></span>--}}


                        {{--</div>--}}
                        {{--<div class="col-sm-5 line-height-35" style="padding-right: 5px;">--}}

                            {{--<a href="{!! route('modals.setting_user_view',['id_select'=>'new_task_users']) !!}" class="jsPanels" title="{{ trans('tasks.selecet_user') }}">--}}
                                {{--<span class="icon icon-afzoodane-fard fonts"></span>--}}
                            {{--</a>--}}
                            {{--<input type="radio" name="assign_type" id="use_type1" class="person_option" value="1" checked/>--}}
                            {{--<label class="person_option" for="use_type1">{{ trans('tasks.collective') }}</label>--}}
                            {{--<input type="radio" name="assign_type" id="use_type2" class="person_option" value="2"/>--}}
                            {{--<label class="person_option" for="use_type2">{{ trans('tasks.individual') }}</label>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                {{--</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
                {{--<td class="width-120">--}}
                    {{--<label class="line-height-35">{{ trans('app.transcript') }}</label>--}}
                {{--</td>--}}
                {{--<td>--}}
                    {{--<div class="row-fluid">--}}
                        {{--<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 row" style="padding-left: 0px;">--}}
                            {{--<select id="new_task_transcripts" name="transcripts[]" class="select2_auto_complete_transcripts"--}}
                                    {{--data-placeholder="{{trans('tasks.select_some_options')}}" multiple></select>--}}
                            {{--<span class=" Chosen-LeftIcon"></span>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 smaller-90 line-height-35" style="padding-right: 5px;">--}}
                            {{--<a href="{!! route('modals.setting_user_view',['id_select'=>'new_task_transcripts']) !!}" class="jsPanels" title="{{ trans('tasks.selecet_user') }}">--}}
                                {{--<span class="icon icon-afzoodane-fard fonts"></span>--}}
                            {{--</a>--}}
                            {{--<input type="checkbox" name="report_on_cr" id="report-type1"/>--}}
                            {{--<label for="">{{ trans('tasks.report_on_task_creation') }}</label>--}}
                            {{--<input type="checkbox" name="report_on_co" id="report-type2"/>--}}
                            {{--<label for="">{{ trans('tasks.report_on_task_completion') }}</label>--}}
                            {{--<input type="checkbox" name="report_to_manager" id="report-type3"/>--}}
                            {{--<label for="">اطلاع به مسئولان</label>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                {{--</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
                {{--<td class="width-120">--}}
                    {{--<label class="line-height-35">{{ trans('app.about') }}</label>--}}
                {{--</td>--}}
                {{--<td>--}}
                    {{--<div class="row-fluid">--}}
                        {{--<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 row">--}}
                            {{--<select id="new_task_pages" class="select2_auto_complete_page " name="pages[]"--}}
                                    {{--data-placeholder="{{trans('tasks.can_select_some_options')}}"--}}
                                    {{--multiple="multiple">--}}
                                {{--@if($sid)--}}
                                    {{--<option value="{{$sid}}" selected>{{$subject->title}}</option>--}}
                                {{--@endif--}}
                            {{--</select>--}}
                            {{--<span class="fa fa-sticky-note Chosen-LeftIcon"></span>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
                {{--<td class="width-120">--}}
                    {{--<label class="line-height-35">{{ trans('tasks.keywords') }}</label>--}}
                {{--</td>--}}
                {{--<td>--}}
                    {{--<div class="row-fluid">--}}
                        {{--<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 row">--}}
                            {{--<select id="new_task_keywords" class="select2_auto_complete_keywords" name="keywords[]"--}}
                                    {{--data-placeholder="{{trans('tasks.can_select_some_options')}}"--}}
                                    {{--multiple="multiple"></select>--}}
                            {{--<span class=" Chosen-LeftIcon"></span>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
                {{--<td></td>--}}
                {{--<td>--}}
                    {{--<div class="row-fluid">--}}
                        {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-inline line-height-35">--}}
                            {{--<div class="form-inline">--}}
                                {{--<input type="checkbox" class="form-control" name="end_on_assigner_accept" id="manager"/>--}}
                                {{--<label for="date">{{ trans('tasks.modal_task_details_assignor_accept_or_ended') }}</label>--}}
                                {{--<input type="checkbox" class="form-control" name="transferable" id="manager"/>--}}
                                {{--<label for="date">{{ trans('tasks.modal_task_details_assignor_to_another') }}</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
                {{--<td class="width-120 ">--}}
                    {{--<label class="line-height-35">{{ trans('app.attachments') }}</label>--}}
                    {{--<div class="row-fluid">--}}
                        {{--<div class="filemanager-buttons-client">--}}
                            {{--<div class="btn btn-default pull-left HFM_ModalOpenBtn" data-section="{{ enCode('CreateNewTask') }}" data-multi_file="Multi" style="margin-right: 0px;">--}}
                                {{--<i class="glyphicon glyphicon-plus-sign" style="color: skyblue"></i>--}}
                                {{--<span>{{trans('app.add_file')}}</span>--}}
                            {{--</div>--}}
                            {{--<div data-section="{{ enCode(session('page_file')) }}"  class="HFM_RemoveAllFileFSS_SubmitBtn btn btn-default pull-left" style=" color:#555;">--}}
                            {{--<i class="glyphicon glyphicon-remove-sign" style=" color:#FF6600;"></i>--}}
                            {{--<span>{{trans('filemanager.remove_all_attachs')}}</span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="pull-right filemanager-title-client">--}}
                            {{--<h4 class="filemanager-title">{{trans('filemanager.attachs')}}</h4>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                {{--</td>--}}
                {{--<td>--}}
                    {{--<div class="row-fluid">--}}
                        {{--{!! $HFM_CN_Task['ShowResultArea']['CreateNewTask'] !!}--}}
                    {{--</div>--}}
                {{--</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
                {{--<td class="width-120">--}}
                    {{--<label class="line-height-35">{{ trans('app.description') }}</label>--}}
                {{--</td>--}}
                {{--<td>--}}
                    {{--<div class="row-fluid">--}}
                        {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 line-height-35">--}}
                            {{--<input type="text" class="form-control row" name="task_desc" id="desc" value="{{@$sel}}"/>--}}
                            {{--<div class="clearfix"></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</td>--}}
            {{--</tr>--}}
        {{--</table>--}}
        {{--<input type="hidden" name="_token" value="{{csrf_token()}}">--}}
        {{--<input type="hidden" name="draft" id="draft" value="0"/>--}}
        {{--<input type="hidden" name="first_m" id="first_m" value="0"/>--}}
        {{--<input type="hidden" name="first_u" id="first_u" value="0"/>--}}
        {{--<input type="hidden" name="assigner_id" value="125"/>--}}
        {{--<input type="hidden" id="save_type" name="save_type" value="0"/>--}}
    {{--</form>--}}
    {{--{!! $HFM_CN_Task['UploadForm'] !!}--}}
{{--</div>--}}
<script>
    $.ajax({
        url: '{{ URL::route('auto_complete.get_user_calendar')}}',
        type: 'Post', // Send post dat
        dataType:'json',
        success: function (s) {

            var options = '';
            $('select[name="event_cid"]').empty();
            for (var i = 0; i < s.length; i++) {
                if(s[i].is_default ==1)
                {
                    options += '<option  selected=true value="' + s[i].id + '">' + s[i].title + '</option>';
                }
                else{
                    options += '<option value="' + s[i].id + '">' + s[i].title + '</option>';
                }


            }

            $('select[name="event_cid"]').append(options);
            $('select[name="event_cid"]').select2({
                dir: "rtl",
                width: '100%',
            });
        }
    });
    $(document).ready(function()
    {
        $(".fileToUpload").on('change', function() {
            var formElement = $( '.fileToUpload' )[0].files[0];
            var data = new FormData();
            data.append('image',formElement);
            data.append('pid','{{rand(1,100).rand(1,100)}}');
            data.append('form_type','form');
            $.ajax
            ({
                url: '{{ route('FileManager.tinymce_external_filemanager') }}',
                type: 'post',
                dataType: 'json',
                data: data,
                processData: false,
                contentType: false,
                success: function(data)
                {
                    console.log(data);
                    if (data.success)
                    {
                        $('#desc').val($('#desc').val() + "\nimg::" + data.FileID + "::img");
                        $('.content_tab_view').html($('#desc').val().replace('img::','<img src="{{route('FileManager.DownloadFile',['type'=> 'ID','id'=>'']).'/'}}').replace('::img','">'));
                    } else
                    {
                        messageModal('fail', 'خطا', data.result);
                    }
                }
            });
        });
        //tab_text tab_view
        $(".tab_desc").on('click', function() {
            $(".tab_desc").removeClass('active');
            $(".content_tab").addClass('hidden');
            $("#for-desc .header").css('height','30px !important');
            $(this).addClass('active');
            $(".content_"+$(this).attr('rel')).removeClass('hidden');
        });
        $(document).on('click', '#form_tinymce_upload .form-submit', function()
        {

        });
        $("#respite_date").val('');
        $("#respite_time").val('');
    });
</script>

<script type="text/javascript" src="{{URL::to('assets/Packages/DataTables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/Packages/bootstrap/js/bootstrap-filestyle.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/Packages/PersianDateOrTimePicker/js/persian-date.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/Packages/PersianDateOrTimePicker/js/persian-datepicker-0.4.5.js')}}"></script>

@include('hamahang.Tasks.helper.CreateNewTask.inline_js')
{!! $HFM_CN_Task['JavaScripts'] !!}