<script>
    $('.first-fix-box').removeClass('height-100');
</script>
<style>
    .content_task {
        top: 71px;
    }
</style>
<div class="col-xs-12 col-md-3 col-sm-6 pdrl-2">
   <div class="text-center div_title_not_started"><h6>{{trans('tasks.status_not_started').' ('.(isset($myTasks['not_started']) ? count($myTasks['not_started']) : 0).')'}}</h6></div>
    <div class="div_groups_task state_container droppable" id="task_notstarted" >
        <ul class="ul_dropp">
            @if(!empty($myTasks['not_started']))
                @foreach($myTasks['not_started'] as $task)
                    <li class="draggable {{$task->RespiteRemain['border_color_class']}}" data-task_id="{{$task->id}}" style="background-color: {{$task->PriorityColor()}} !important;">
                        <div class="header_div_list_task container-fluid prl-1 text_ellipsis">
                            <span class="div_img">
                                @if(strstr(\Route::currentRouteName(),'hamahang.tasks.my_assigned_tasks.state'))
                                    @php
                                        $do = 'ViewTaskForm';
                                        $employee = $task->Assignments[0];
                                    @endphp
{{--                                    @foreach($task->Assignments as $employee)--}}
                                        @if(isset($employee->Employee))
                                            <span class="referrer" style="top: 1px;" data-toggle="tooltip" title="{{trans('tasks.responsible').': '.$employee->Employee->Name . ' ' . $employee->Employee->Family}}">
                                                <a href="{{url($employee->Employee->Uname)}}" title="{{$employee->Employee->Name . ' ' . $employee->Employee->Family}}">
                                                    <i >{!! $employee->Employee->BetweenSmallandBig !!}</i>
                                                </a>
                                            </span>
                                        @endif
                                    {{--@endforeach--}}
                                @else
                                    @php
                                        $do = 'ViewTaskForm';
                                    @endphp
                                    <span class="referrer" style="top: 1px;" data-toggle="tooltip" title="{{trans('tasks.reffered').': '.$task->Assignment->Assigner->Name . ' ' . $task->Assignment->Assigner->Family}}">
                                        <a href="{{url($task->Assignment->Assigner->Uname)}}" title="{{$task->Assignment->Assigner->Name . ' ' . $task->Assignment->Assigner->Family}}">
                                            <i >{!! $task->Assignment->Assigner->BetweenSmallandBig !!}</i>
                                        </a>
                                    </span>
                                @endif
                            </span>
                            <span class="">
                                <span data-toggle="tooltip" title="{{$task->title."\n".$task->desc}}" class="text_ellipsis" style="margin: 0px 5px">
                                    <a class='cursor-pointer jsPanels' href='/modals/{{$do}}?tid={{enCode($task->id)}}&aid={{$task->Assignment->id}}'>
                                        {{--@php--}}
                                            {{--$msgTrimmed = preg_split('/ /',$task->title);--}}
                                            {{--$cnt = 0;--}}
                                            {{--$sub_title = '';--}}
                                            {{--foreach($msgTrimmed as $word){--}}
                                                {{--if($cnt++ <=5){--}}
                                                    {{--$sub_title .= " $word";--}}
                                                {{--}else{--}}
                                                    {{--$sub_title .= '...';--}}
                                                    {{--break;--}}
                                                {{--}--}}
                                            {{--}--}}
                                        {{--@endphp--}}
                                        {{$task->title}}
                                    </a>
                                </span>
                            </span>
                            <a href='{{ URL::route('hamahang.calendar_events.reminder_modal' )}}{{$task->RespiteRemain['url']}}' style='display: contents;' class='list-group-item line-height-30 height-30 jsPanels'>
                                <div style="" data-toggle="tooltip" title="{{$task->RespiteRemain['gdate']}}" class="respite_number_task_state {{$task->RespiteRemain['bg_color_class']}}">{{$task->RespiteRemain['days']}}</div>
                            </a>
                        </div>
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</div>
<div class="col-xs-12 col-md-3 col-sm-6 pdrl-2">
    <div class="text-center"><h6>{{trans('tasks.status_started').' ('.(isset($myTasks['started']) ? count($myTasks['started']) : 0).')'}}</h6></div>
    <div class="div_groups_task state_container droppable" id="task_started" >
        <ul class="ul_dropp">
            @if(!empty($myTasks['started']))
                @foreach($myTasks['started'] as $task)
                    <li class="draggable {{$task->RespiteRemain['border_color_class']}}" data-task_id="{{$task->id}}" style="background-color: {{$task->PriorityColor()}} !important;">
                        <div class="header_div_list_task container-fluid prl-1 text_ellipsis">
                            <span class="div_img">
                                @if(strstr(\Route::currentRouteName(),'hamahang.tasks.my_assigned_tasks.state'))
                                    @php
                                        $do = 'ViewTaskForm';
                                        $employee = $task->Assignments[0];
                                    @endphp
                                    {{--@foreach($task->Assignments as $employee)--}}
                                    @if(isset($employee->Employee))
                                        <span class="referrer" style="top: 1px;" data-toggle="tooltip" title="{{trans('tasks.responsible').': '.$employee->Employee->Name . ' ' . $employee->Employee->Family}}">
                                                <a href="{{url($employee->Employee->Uname)}}" title="{{$employee->Employee->Name . ' ' . $employee->Employee->Family}}">
                                                    <i >{!! $employee->Employee->BetweenSmallandBig !!}</i>
                                                </a>
                                            </span>
                                    @endif
                                    {{--@endforeach--}}
                                @else
                                    @php
                                        $do = 'ViewTaskForm';
                                    @endphp
                                    <span class="referrer" style="top: 1px;" data-toggle="tooltip" title="{{trans('tasks.reffered').': '.$task->Assignment->Assigner->Name . ' ' . $task->Assignment->Assigner->Family}}">
                                        <a href="{{url($task->Assignment->Assigner->Uname)}}" title="{{$task->Assignment->Assigner->Name . ' ' . $task->Assignment->Assigner->Family}}">
                                            <i >{!! $task->Assignment->Assigner->BetweenSmallandBig !!}</i>
                                        </a>
                                    </span>
                                @endif
                            </span>
                            <span >
                                <span data-toggle="tooltip" title="{{$task->title."\n".$task->desc}}" class="">
                                    <a class='cursor-pointer jsPanels' href='/modals/{{$do}}?tid={{enCode($task->id)}}&aid={{$task->Assignment->id}}'>
                                        {{--@php--}}
                                            {{--$msgTrimmed = preg_split('/ /',$task->title);--}}
                                            {{--$cnt = 0;--}}
                                            {{--$sub_title = '';--}}
                                            {{--foreach($msgTrimmed as $word){--}}
                                                {{--if($cnt++ <=5){--}}
                                                    {{--$sub_title .= " $word";--}}
                                                {{--}else{--}}
                                                    {{--$sub_title .= '...';--}}
                                                    {{--break;--}}
                                                {{--}--}}
                                            {{--}--}}
                                        {{--@endphp--}}
                                        {{$task->title}}
                                    </a>
                                </span>
                            </span>
                            <a href='{{ URL::route('hamahang.calendar_events.reminder_modal' )}}{{$task->RespiteRemain['url']}}' style='display: contents;' class='list-group-item line-height-30 height-30 jsPanels'>
                                <div style="" data-toggle="tooltip" title="{{$task->RespiteRemain['gdate']}}" class="respite_number_task_state {{$task->RespiteRemain['bg_color_class']}}">{{$task->RespiteRemain['days']}}</div>
                            </a>
                        </div>
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</div>
<div class="col-xs-12 col-md-3 col-sm-6 pdrl-2">
    <div class="text-center"><h6>{{trans('tasks.status_done').' ('.(isset($myTasks['done']) ? count($myTasks['done']) : 0).')'}}</h6></div>
    <div class="div_groups_task state_container droppable" id="task_done">
        <ul class="ul_dropp">
            @if(!empty($myTasks['done']))
                @foreach($myTasks['done'] as $task)
                    <li class="draggable {{$task->RespiteRemain['border_color_class']}}" data-task_id="{{$task->id}}" style="background-color: {{$task->PriorityColor()}} !important;">
                        <div class="header_div_list_task container-fluid prl-1 text_ellipsis">
                            <span class="div_img">
                                @if(strstr(\Route::currentRouteName(),'hamahang.tasks.my_assigned_tasks.state'))
                                    @php
                                        $do = 'ViewTaskForm';
                                        $employee = $task->Assignments[0];
                                    @endphp
                                    @if(isset($employee->Employee))
                                        <span class="referrer" style="top: 1px;" data-toggle="tooltip" title="{{trans('tasks.responsible').': '.$employee->Employee->Name . ' ' . $employee->Employee->Family}}">
                                                <a href="{{url($employee->Employee->Uname)}}" title="{{$employee->Employee->Name . ' ' . $employee->Employee->Family}}">
                                                    <i >{!! $employee->Employee->BetweenSmallandBig !!}</i>
                                                </a>
                                            </span>
                                    @endif
                                @else
                                    @php
                                        $do = 'ViewTaskForm';
                                    @endphp
                                    <span class="referrer" style="top: 1px;" data-toggle="tooltip" title="{{trans('tasks.reffered').': '.$task->Assignment->Assigner->Name . ' ' . $task->Assignment->Assigner->Family}}">
                                        <a href="{{url($task->Assignment->Assigner->Uname)}}" title="{{$task->Assignment->Assigner->Name . ' ' . $task->Assignment->Assigner->Family}}">
                                            <i >{!! $task->Assignment->Assigner->BetweenSmallandBig !!}</i>
                                        </a>
                                    </span>
                                @endif
                            </span>
                            <span >
                                <span data-toggle="tooltip" title="{{$task->title."\n".$task->desc}}" class="">
                                    <a class='cursor-pointer jsPanels' href='/modals/{{$do}}?tid={{enCode($task->id)}}&aid={{$task->Assignment->id}}'>
                                        {{--@php--}}
                                            {{--$msgTrimmed = preg_split('/ /',$task->title);--}}
                                            {{--$cnt = 0;--}}
                                            {{--$sub_title = '';--}}
                                            {{--foreach($msgTrimmed as $word){--}}
                                                {{--if($cnt++ <=5){--}}
                                                    {{--$sub_title .= " $word";--}}
                                                {{--}else{--}}
                                                    {{--$sub_title .= '...';--}}
                                                    {{--break;--}}
                                                {{--}--}}
                                            {{--}--}}
                                        {{--@endphp--}}
                                        {{$task->title}}
                                    </a>
                                </span>
                            </span>
                            <a href='{{ URL::route('hamahang.calendar_events.reminder_modal' )}}{{$task->RespiteRemain['url']}}' style='display: contents;' class='list-group-item line-height-30 height-30 jsPanels'>
                                <div style="" data-toggle="tooltip" title="{{$task->RespiteRemain['gdate']}}" class="respite_number_task_state {{$task->RespiteRemain['bg_color_class']}}">{{$task->RespiteRemain['days']}}</div>
                            </a>
                        </div>
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</div>
<div class="col-xs-12 col-md-3 col-sm-6 pdrl-2">
    <div class="text-center"><h6>{{trans('tasks.status_finished').' ('.(isset($myTasks['ended']) ? count($myTasks['ended']) : 0).')'}}</h6></div>
    <div class="div_groups_task state_container droppable" id="task_ended">
        <ul class="ul_dropp">
            @if(!empty($myTasks['ended']))
                @foreach($myTasks['ended'] as $task)
                    <li class="draggable {{$task->RespiteRemain['border_color_class']}}" data-task_id="{{$task->id}}" style="background-color: {{$task->PriorityColor()}} !important;">
                        <div class="header_div_list_task container-fluid prl-1 text_ellipsis">
                            <span class="div_img">
                                @if(strstr(\Route::currentRouteName(),'hamahang.tasks.my_assigned_tasks.state'))
                                    @php
                                        $do = 'ViewTaskForm';
                                        $employee = $task->Assignments[0];
                                    @endphp
                                    @if(isset($employee->Employee))
                                        <span class="referrer" style="top: 1px;" data-toggle="tooltip" title="{{trans('tasks.responsible').': '.$employee->Employee->Name . ' ' . $employee->Employee->Family}}">
                                                <a href="{{url($employee->Employee->Uname)}}" title="{{$employee->Employee->Name . ' ' . $employee->Employee->Family}}">
                                                    <i >{!! $employee->Employee->BetweenSmallandBig !!}</i>
                                                </a>
                                            </span>
                                    @endif
                                @else
                                    @php
                                        $do = 'ViewTaskForm';
                                    @endphp
                                    <span class="referrer" style="top: 1px;" data-toggle="tooltip" title="{{trans('tasks.reffered').': '.$task->Assignment->Assigner->Name . ' ' . $task->Assignment->Assigner->Family}}">
                                        <a href="{{url($task->Assignment->Assigner->Uname)}}" title="{{$task->Assignment->Assigner->Name . ' ' . $task->Assignment->Assigner->Family}}">
                                            <i >{!! $task->Assignment->Assigner->BetweenSmallandBig !!}</i>
                                        </a>
                                    </span>
                                @endif
                            </span>
                            <span >
                                <span data-toggle="tooltip" title="{{$task->title."\n".$task->desc}}" class="">
                                    <a class='cursor-pointer jsPanels' href='/modals/{{$do}}?tid={{enCode($task->id)}}&aid={{$task->Assignment->id}}'>
                                        {{--@php--}}
                                            {{--$msgTrimmed = preg_split('/ /',$task->title);--}}
                                            {{--$cnt = 0;--}}
                                            {{--$sub_title = '';--}}
                                            {{--foreach($msgTrimmed as $word){--}}
                                                {{--if($cnt++ <=5){--}}
                                                    {{--$sub_title .= " $word";--}}
                                                {{--}else{--}}
                                                    {{--$sub_title .= '...';--}}
                                                    {{--break;--}}
                                                {{--}--}}
                                            {{--}--}}
                                        {{--@endphp--}}
                                        {{$task->title}}
                                    </a>
                                </span>
                            </span>
                            <a href='{{ URL::route('hamahang.calendar_events.reminder_modal' )}}{{$task->RespiteRemain['url']}}' style='display: contents;' class='list-group-item line-height-30 height-30 jsPanels'>
                                <div style="" data-toggle="tooltip" title="{{$task->RespiteRemain['gdate']}}" class="respite_number_task_state {{$task->RespiteRemain['bg_color_class']}}">{{$task->RespiteRemain['days']}}</div>
                            </a>
                        </div>
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</div>