<table id="coupons_grid" class="table table-bordered table-striped td-center-align" cellspacing="0" width="100%">
    <thead>
        <th style="text-align: center; padding-right: 7px;"><small>{{ trans('bazaar.discountcoupon.row') }}</small></th>
        <th style="text-align: center; padding-right: 7px;"><small>{{ trans('bazaar.discountcoupon.coupon') }}</small></th>
        <th style="text-align: center; padding-right: 7px;"><small>{{ trans('bazaar.discountcoupon.type') }}</small></th>
        <th style="text-align: center; padding-right: 7px;"><small>{{ trans('bazaar.discountcoupon.value') }}</small></th>
        <th style="text-align: center; padding-right: 7px;"><small>{{ trans('bazaar.discountcoupon.start_date') }}</small></th>
        <th style="text-align: center; padding-right: 7px;"><small>{{ trans('bazaar.discountcoupon.expire_date') }}</small></th>
        <th style="text-align: center; padding-right: 7px;"><small>{{ trans('bazaar.discountcoupon.disposable') }}</small></th>
        <th style="text-align: center; padding-right: 7px;"><small>{{ trans('bazaar.discountcoupon.usage_quota') }}</small></th>
        <th style="text-align: center; padding-right: 7px;"><small>{{ trans('bazaar.discountcoupon.used_count') }}</small></th>
        <th style="text-align: center; padding-right: 7px;"><small>{{ trans('bazaar.discountcoupon.status') }}</small></th>
        <th style="text-align: center; padding-right: 7px;"><small>{{ trans('bazaar.discountcoupon.operations') }}</small></th>
    </thead>
</table>
