<style>
    .operation-edit,
    .operation-delete
    {
        /*border-radius: 5px;*/
        cursor: pointer;
        height: 25px;
        width: 25px;
        padding: 5px;
    }
    .operation-edit
    {
        background-color: #eeeeee;
    }
    .operation-delete
    {
        background-color: #ffedee;
        color: #f05457;
    }
</style>