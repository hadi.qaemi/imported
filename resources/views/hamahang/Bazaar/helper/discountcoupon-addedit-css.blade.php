<style>
    .table_coupon.table,
    .table_coupon.table > thead > tr > th,
    .table_coupon.table > tbody > tr > th,
    .table_coupon.table > tfoot > tr > th,
    .table_coupon.table > thead > tr > td,
    .table_coupon.table > tbody > tr > td,
    .table_coupon.table > tfoot > tr > td
    {
        border: none;
        vertical-align: middle;
    }
    .table_coupon.table
    {
        margin-bottom: 0;
    }
</style>