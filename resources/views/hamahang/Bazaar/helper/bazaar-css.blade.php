<style>
    .h-content
    {

    }
    .h-dropdown-h
    {
        cursor: pointer;
    }
    .h-dropdown-list
    {
        display: none;
    }
    .h-dropdown-list div
    {
        padding: 10px 30px;
    }
    table tr,
    table tr td,
    input[type=checkbox],
    input[type=radio]
    {
        margin-right: 0;
        padding-right: 0;
    }
    table tr td
    {
        vertical-align: middle !important;
    }
    .form_bazaar span.larg
    {
        font-size: 18px;
    }
    .form_bazaar span.small
    {
        font-size: 6px;
    }
    ol.progress-track
    {
        display: table;
        list-style-type: none;
        margin: 0;
        padding: 2em 1em;
        table-layout: fixed;
        width: 100%;
    }
    ol.progress-track li
    {
        display: table-cell;
        line-height: 3em;
        position: relative;
        text-align: center;
    }
    ol.progress-track li .icon-wrap
    {
        border-radius: 50%;
        top: -1.5em;
        color: #fff;
        display: block;
        height: 2.5em;
        margin: 0 auto -2em;
        left: 0;
        right: 0;
        position: absolute;
        width: 2.5em;
    }
    ol.progress-track li .icon-check-mark,
    ol.progress-track li .icon-down-arrow
    {
        height: 25px;
        width: 15px;
        display: inline-block;
        fill: currentColor;
    }
    ol.progress-track li .progress-text
    {
        position: relative;
        top: 10px;
    }
    ol.progress-track li.progress-done
    {
        border-top: 3px solid #87BA51;
        transition: border-color 1s ease-in-out;
        -webkit-transition: border-color 1s ease-in-out;
        -moz-transition: border-color 1s ease-in-out;
    }
    ol.progress-track li.progress-done .icon-down-arrow
    {
        display: none;
    }
    ol.progress-track li.progress-done.progress-current .icon-wrap
    {
        background-color: #EBFFEB;
    }
    ol.progress-track li.progress-done.progress-current .icon-wrap .icon-check-mark
    {
        display: none;
    }
    ol.progress-track li.progress-done.progress-current .icon-wrap .icon-down-arrow
    {
        display: block;
    }
    ol.progress-track li.progress-done .icon-wrap
    {
        background-color: #87BA51;
        border: 5px solid #87ba51;
    }
    ol.progress-track li.progress-todo
    {
        border-top: 3px solid #DDD;
        color: black;
    }
    ol.progress-track li.progress-todo .icon-wrap
    {
        background-color: #FFF;
        border: 5px solid #DDD;
        border-radius: 50%;
        bottom: 1.5em;
        color: #fff;
        display: block;
        height: 2.5em;
        margin: 0 auto -2em;
        position: relative;
        width: 2.5em;
    }
    ol.progress-track li.progress-todo .icon-wrap .icon-check-mark,
    ol.progress-track li.progress-todo .icon-wrap .icon-down-arrow
    {
        display: none;
    }
    .table,
    .table-bordered > thead > tr > th,
    .table-bordered > tbody > tr > th,
    .table-bordered > tfoot > tr > th,
    .table-bordered > thead > tr > td,
    .table-bordered > tbody > tr > td,
    .table-bordered > tfoot > tr > td
    {
        border: 1px solid #F0F1F2;
        color: #666666;
        vertical-align: middle;
    }
    table.table-bordered thead tr td
    {
        padding-top: 10px;
    }
    .h-close
    {
        background-color: #FFEDEE;
    }
    .h-close .fa-close
    {
        color: #F05457;
        cursor: pointer;
        font-size: 17px;
    }

</style>

