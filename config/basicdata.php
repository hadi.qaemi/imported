<?php

return
[
    'social_attr_image' => env('BASIC_DATA_social_attr_image', 11),
    'social_attr_link' => env('BASIC_DATA_social_attr_link', 10),
    'research_attr_link' => env('BASIC_DATA_research_attr_link', 12),
    'research_attr_image' => env('BASIC_DATA_research_attr_image', 13),
    'kmkz' =>
    [
        'homepage_link_group_id' => env('CONSTANTS_HOMEPAGE_LINK_GROUP_ID', 9),
    ],
];