<?php

return [
    'available_sections' =>
    [
        'login',
        'register',
        'remember_password'
    ],
    'length'=>'5',
    'quality'=>'100',
    'width'=>'200',
    'height'=>'70',
    'color'=>'#eee',
    'CAPTCHA_FONT_ADDRESS'=>'assets/fonts/captcha_font.otf'
];
