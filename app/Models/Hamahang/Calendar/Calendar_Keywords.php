<?php
/**
 * Created by PhpStorm.
 * User: hamahang
 * Date: 11/27/16
 * Time: 12:39 PM
 */
namespace App\Models\Hamahang\Calendar;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Calendar_Keywords extends Model
{
    use SoftDeletes;
    protected $table = 'hamahang_calendar_keywords';
    protected $dates = ['deleted_at'];
    protected $guarded = [];


    public function calendar(){
        return $this->belongsTo('App\Models\Hamahang\Calendar\Calendar','calendar_id','id');
    }

    public function keyword()
    {
        return $this->belongsTo('App\Models\Hamahang\keywords','keyword_id','id');
    }
}