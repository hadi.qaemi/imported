<?php


namespace App\Models\Hamahang\CalendarEvents;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Sessions_Members extends Model
{
    use SoftDeletes;
    protected $table = "hamahang_calendar_sessions_members";
    protected $dates = ['deleted_at'];
    protected $guarded = [];


    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }

    public function session(){
        return $this->belongsTo('App\Models\Hamahang\CalendarEvents\Session_Events','session_id','id');
    }

    public function role(){
        if($this->uid == auth()->id()){
            return trans('calendar_events.ce_moda_session_modal_session_creator');
        }else{
            switch ($this->user_type)
            {
                case 2:
                    {
                        return trans('calendar_events.ce_moda_session_modal_session_notvoting_users');
                        break;
                    }
                case 1:
                    {
                        return trans('calendar_events.ce_moda_session_modal_session_voting_users');
                        break;
                    }
                case 3:
                    {
                        return trans('calendar_events.ce_moda_session_modal_facilitator');
                        break;
                    }
                case 4:
                    {
                        return trans('calendar_events.ce_moda_session_modal_session_secretary');
                        break;
                    }
                case 5:
                    {
                        return trans('calendar_events.ce_moda_session_modal_session_chief');
                        break;
                    }
                default:
                    {

                        break;
                    }
            }
        }
    }


}