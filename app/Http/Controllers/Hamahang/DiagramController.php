<?php
namespace App\Http\Controllers\Hamahang;


use App\Models\Hamahang\diagrams;

use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;

class DiagramController extends Controller
{

    public function diagram_list_all($uname)
    {
        $arr = variable_generator('user', 'desktop', $uname);
        return view('hamahang.Diagram.DiagramList', $arr);
    }

    public function fetech_all_diagram()
    {
//        dd(\Request::all());
        $diagrams = diagrams::with('keywords', 'users_permissions');
        if(\Request::exists('title'))
        {
            $diagrams->where('title', 'like', '%'.\Request::get('title').'%');
        }
        if((\Request::exists('keywords')))
        {
            $keywords = [];
            foreach (\Request::get('keywords') as $keyword)
            {
                $keywords[] = str_replace('exist_in','',$keyword);
            }
            $diagrams->whereHas('keywords', function ($query) use ($keywords)
            {
                $query->whereIn('keyword_id', $keywords);
            });
        }
        if((\Request::exists('users')))
        {
            $users = \Request::get('users');
            $diagrams->whereHas('users_permissions', function ($query) use ($users)
            {
                $query->whereIn('user_id', $users);
            });
        }

        return Datatables::eloquent($diagrams)
            ->addColumn('keywords', function ($data)
            {
                $rr = [];

                if(is_object($data->keywords))
                {
                    foreach($data->keywords as $Ar)
                        $rr[]= ['id'=>$Ar->keyword->id,'title'=>$Ar->keyword->title];
                }
                return json_encode($rr);
            })
            ->addColumn('users', function ($data)
            {
                $rr = [];

                if(isset($data->users_permissions))
                {
                    foreach($data->users_permissions as $Ar)
                        if(isset($Ar->user->Uname) && $Ar->user->Name)
                            $rr[]= ['id'=>$Ar->user->Uname,'title'=>$Ar->user->Name.' '.$Ar->user->Family];
                }
                return json_encode($rr);
            })
            ->addColumn('id', function ($data)
            {
                return enCode($data->id);
            })
            ->addColumn('did', function ($data)
            {
                return ($data->id);
            })
            ->make(true);


    }

    public function save_diagram()
    {
        $diagrams = diagrams::find(deCode(\Request::get('did')));
        $diagrams->keywords()->delete();
        if(\Request::exists('keywords'))
        {
            foreach (\Request::get('keywords') as $keyword)
            {
                $keyword = str_replace('exist_in','',$keyword);
                $diagrams->keywords()->create([
                    'uid' => auth()->user()->id,
                    'keyword_id' => $keyword
                ]);
            }
        }
        $diagrams->users_permissions()->delete();
        if(\Request::exists('users_list_subject_view'))
        {
            foreach (\Request::get('users_list_subject_view') as $user)
            {
                $diagrams->users_permissions()->create([
                    'uid' => auth()->user()->id,
                    'user_id' => $user
                ]);
            }
        }
        $diagrams->title = \Request::get('title');
        if($diagrams->save())
            return json_encode(['status' => true]);

    }
}