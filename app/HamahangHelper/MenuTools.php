<?php

use App\Models\hamafza\Subject;

if (!function_exists('buildMenuTree'))
{
    /**
     * @param $flat_array
     * @param $pidKey
     * @param $current_url
     * @param $item
     * @param int $parent
     * @param string $idKey
     * @param string $children_key
     * @return mixed
     */
    function buildMenuTree($flat_array, $pidKey, $item = false, $current_url = '/', $parent = 0, $idKey = 'id', $children_key = 'children')
    {
        $grouped = array();
        foreach ($flat_array as $sub)
        {
            $sub['text'] = $sub['title'];
            if ($sub['href'] != '' && $sub['href_type'] == 0)
            {
                $sub['a_attr']['href'] = '#';
                $sub['a_attr']['hrefhref'] = $sub['href'];
            }
            else
            {
                if ($sub['href'] != '' && $sub['href_type'] == 1 && Auth::check())
                {
                    $sub['href'] = str_replace('[username]', Auth::user()->Uname, $sub['href']);
                    if ($item)
                    {
                        $sub['href'] = str_replace('[subject_id]', $item, $sub['href']);
                        $sub['href'] = str_replace('[page_id]', $item, $sub['href']);
                    }
                    $sub['a_attr']['href'] = '#';
                    $sub['a_attr']['hrefhref'] =  url($sub['href']);
                }
                else
                {
                    $route_var = json_decode($sub['route_variable']);
                    /* @FixedMe change username var to current login user */
                    if ($route_var != null)
                    {
                        $result_route_var = array();
                        foreach ($route_var as $rk => $rv)
                        {
                            //$rv =json_decode($rv);
                            if (isset($rv->username) || empty($rv->username))
                            {
                                $result_route_var[$rk] = Auth::user()->Uname;
                            }
                        }

                        $sub['a_attr']['href'] = '#';
                        $sub['a_attr']['hrefhref'] = route($sub['route_name'], $result_route_var);
                    }
                    else
                    {
                        $sub['a_attr']['href'] = '#';
                        $sub['a_attr']['hrefhref'] = $sub['href'];
                    }
                }
            }
            if ($current_url == $sub['a_attr']['hrefhref'])
            {
                //var_dump($sub['href']);
                $sub['state']['opened'] = true;
                $sub['state']['selected'] = true;
            }
            $menu_items = \Session::get('menu_items');
            if(isset($menu_items[$sub['id']]))
            {
                $menu_item = $menu_items[$sub['id']];
            }else{
                $menu_item = \App\Models\Hamahang\Menus\MenuItem::find($sub['id']);
                $menu_items[$sub['id']] = $menu_item;
//                \Session::put('menu_items',$menu_items);
            }

            $policyObj = new \App\Policies\MenuPolicy();
            if (Auth::check())
            {
                $user = Auth::user();
            }
            else
            {
                $user = new \App\User();
            }
            $menu_canView = \Session::get('menu_canView');
            $menu_item_enCode = enCode($menu_item);
            if(isset($menu_canView[$menu_item_enCode]))
            {
                $access = $menu_canView[$menu_item_enCode];
            }else{

                $access = $policyObj->canView($user, $menu_item);
                $menu_canView[$menu_item_enCode] = $access;
//                \Session::put('menu_canView',$menu_canView);
            }

            if ($access)
            {
                $grouped[$sub[$pidKey]][] = $sub;
            }
        }

        $fnBuilder = function ($siblings) use (&$fnBuilder, $grouped, $idKey, $children_key)
        {
            $siblings = sort_arr($siblings);
            foreach ($siblings as $k => $sibling)
            {
                $id = $sibling[$idKey];
                if (isset($grouped[$id]))
                {
                    $sibling[$children_key] = $fnBuilder($grouped[$id]);
                }
                $siblings[$k] = $sibling;
            }
            return $siblings;
        };
        if (isset($grouped[$parent]))
        {
            $tree = $fnBuilder($grouped[$parent]);
        }
        else
        {
            $tree = [];
        }

        $tree_t = '';
        foreach($tree as $k=>$node) {
            $tree_child = '';
            $status = 'hide';
            if(isset($node['children']))
            {
                foreach ($node['children'] as $children) {
                    if(isset($children['state']) and isset($children['state']['opened']))
                    {
                        if($children['state']['opened']==true)
                        {
                            $status = 'ShowIndex show';
                            $tree_child .= '<li class="active"><a href="/' . $children['href'] . '">' . $children['title'] . '</a></li>';
                        }
                    }else{
                        $tree_child .= '<li><a href="/' . $children['href'] . '">' . $children['title'] . '</a></li>';
                    }
                }
            }
            $tree_t .= '<li class="dropdown-ver" node="'.$k.'">';
            $tree_t .= '<a href="' . $node['href'] . '" data-toggle="dropdown-ver" class="col-xs-12"><span class="col-xs-11">' . $node['title'] . '</span><span class="col-xs-1 submenu-icon fa fa-angle-down"></span></a>';
            $tree_t .= '<ul class="dropdown-menu node'.$k.' '.$status.'">';
            $tree_t .= $tree_child;
            $tree_t .= '</ul>';
            $tree_t .= '</li>';
        }
        return $tree_t;
    }

}
if (!function_exists('html_ordered_menu'))
{
    function html_ordered_menu($array,$parent_id = 0,$class="")
    {
        $menu_html = '<ul '.(trim($class) == '' ? '' : "class='".$class."'").'>';
        foreach($array as $element)
        {
            if($element['parent_id']==$parent_id)
            {
                $html_ordered_menu = html_ordered_menu($array,$element['id']);
                $element['href'] = (stristr($element['href'],'/') ? '' : '/').$element['href'];

                $strlen = strlen(trim($html_ordered_menu));
                $menu_html .= '<li class="'.($strlen>10 ? 'parent_li li_'.($parent_id==0 ? 'root' : 'parent') : '').'">'.($strlen>10 ? ($parent_id==0 ? '<span class="fa fa-sort-down display-inline padding-left-10"></span>' : '<span class="fa fa-caret-left display-inline padding-left-10"></span>') : '').'<a href="'.$element['href'].'" class="display-inline">'.$element['title'].'</a>';
                if($strlen>10)
                    $menu_html .= $html_ordered_menu;

                $menu_html .= '</li>';
            }
        }

        $menu_html .= '</ul>';
        return $menu_html;
    }
}
if (!function_exists('menuGenerator'))
{
    /**
     * @param $menu_type_id int
     * @param $subject_id int
     * @param $option String
     * @return $this
     */
    function menuGenerator($menu_type_id, $type, $subject_id = false)
    {
        switch ($type)
        {
            case 'tree';
                $uname = auth()->user();
                return view('hamahang.Menus.helper.treemenu')
                    ->with('uname', $uname)
                    ->with('type_id', $menu_type_id)
                    ->with('subject_id', $subject_id);
                break;
            case 'horizontal':
                $menuObj = \App\Models\Hamahang\Menus\Menus::find($menu_type_id);
                $menus = $menuObj->items()->where('status', '1')->get();
                return view('hamahang.Menus.helper.horizental_menu')->with('menus', $menus);
                break;
            case 'footer':
                $menuObj = \App\Models\Hamahang\Menus\Menus::find($menu_type_id);
                if ($menuObj)
                {
                    $menus = $menuObj->items()->where('status', '1')->get();
                }
                else
                {
                    $menus = false;
                }
                return view('hamahang.Menus.helper.menu_horizental_footer')->with('menus', $menus);
                break;
        }
    }
}
if (!function_exists('toolsGenerator'))
{
    /**
     * @param $options array
     * @param $option array
     * @param $position int
     * @param $relation int
     * @return $this
     */
    function toolsGenerator($options, $position, $relation = 4, $option = ['subject_id' => '[subject_id]', 'page_id' => '[page_id]'])
    {
        $option_ids = [];
        $needed_variables = [];
        foreach ($options as $key_option => $value_opt)
        {
            $option_ids[] = $key_option;
            $needed_variables = array_merge($needed_variables, $value_opt);
        }
        $i = 0;
        $more_nv = '';
        $get_url_str = '?';
        foreach ($needed_variables as $nv_key => $nv_value)
        {
            if ($i > 0)
            {
                $more_nv = '&';
            }
            $get_url_str .= $more_nv . $nv_key . '=' . $nv_value;
            $i++;
        }
        $groups = \App\Models\Hamahang\Tools\ToolsGroup::where('visible', '=', "1")
            ->with(
                [
                    'tools' => function ($query) use ($option_ids)
                    {
                        $query->whereHas('options', function ($query) use ($option_ids)
                        {
                            $query->whereIn('hamahang_options.id', $option_ids);
                        })->where('visible', "1");
                    },
                    'tools.options' => function ($query) use ($option_ids)
                    {
                        $query->whereIn('hamahang_options.id', $option_ids);
                    }
                ])
            ->whereHas('tools', function ($query) use ($position)
            {
                $query->whereHas('positions', function ($query) use ($position)
                {
                    $query->where('hamahang_template_positions.id', '=', $position);
                })->where('visible', "1");
            })
            ->get();
        return view('hamahang.Tools.helper.tools_generator')
            ->with('groups', $groups)
            ->with('get_url_str', $get_url_str)
            ->with('option', $option);
    }
}
if (!function_exists('policy_CanView'))
{
    /**
     * @param $options array
     * @param $option array
     * @param $position int
     * @param $relation int
     * @return $this
     */
    function policy_CanView($id = '', $Model, $ModelPolicy, $PolicyMethod = 'canView', $abort = false)
    {
        $item = '';
        if ($id != '')
        {
            $item = $Model::find($id);
        }

        $ResModelPolicy = new $ModelPolicy();
        // $policyObj = new $addr_policy;
        if (Auth::check())
        {
            $user = Auth::user();
        }
        else
        {
            $user = new \App\User();
        }

        $access = $ResModelPolicy->$PolicyMethod($user, $item);

        if (!$access && $abort)
        {
            return false;
//            abort($abort);
        }
        if($id!='' && $Model=='\App\Models\hamafza\Subject')
        {
			$Subjects2 =
					DB::table('hamahang_role_policies as hrp')
						->where('hrp.role_id', 3)
						->where('hrp.target_id', $id)
						->select('hrp.target_id as target_id')->take(50)->get();
			
			if(count($Subjects2)==0){
				if (Auth::check())
				{
					$Subjects =
					DB::table('hamahang_user_policies as hup')
						->leftJoin('user as u', 'hup.user_id', '=', 'u.id')
						->where('u.id', $user->id)
						->where('hup.target_id', $id)
						->select('u.Uname as name')->take(50)->get();
						
					$Subjects2 =
						DB::table('hamahang_role_policies as hrp')
							->leftJoin('role_user as ru', 'ru.role_id', '=', 'hrp.role_id')
							->leftJoin('user as u', 'u.id', '=', 'ru.user_id')
							->where('u.id', $user->id)
							->where('hrp.target_id', $id)
							->select('u.Uname as name')->take(50)->get();
					if(count($Subjects)==0 && count($Subjects2)==0)
					{
                        return false;
//						abort($abort);
					}
				}
				else
				{
					$Subjects2 =
						DB::table('hamahang_role_policies as hrp')
							->where('hrp.role_id', 3)
							->where('hrp.target_id', $id)
							->select('hrp.target_id as target_id')->take(50)->get();
					if(count($Subjects2)==0)
					{
                        return false;
//						abort($abort);
					}
				}
			}
			
            

        }

        return $access;
    }
}

if (!function_exists('getShortToolDetail'))
{
    /**
     * @param $type int 1:subject ,2:user
     * @param $id int
     * @return $this
     */
    function getShortToolDetail($type = 0, $id = 0)
    {
        switch ($type)
        {
            case 'Page':
                $pageDet = DB::table('subject_member')
                    ->select('id', 'relation', 'follow', 'like')
                    ->where('uid', \Auth::id())
                    ->where('sid', $id)
                    ->first();
                break;
            case 'User':
            {
                $pageDet = DB::table('user_friend')
                    ->select('id', 'relation', 'follow', 'like')
                    ->where('uid', \Auth::id())
                    ->where('fid', $id)
                    ->first();
                break;
            }
            case 'Group':
            {
                $pageDet = DB::table('user_group_member')
                    ->select('id', 'relation', 'follow', 'like')
                    ->where('uid', \Auth::id())
                    ->where('gid', $id)
                    ->first();
            }
        }
        $res = array();
        if ($pageDet)
        {
            $res['like'] = $pageDet->like;
            $res['follow'] = $pageDet->follow;
            $res['relation'] = $pageDet->relation;
        }
        else
        {
            $res['like'] = '0';
            $res['follow'] = 0;
            $res['relation'] = 0;
        }
        return $res;
    }

}
if (!function_exists('shortToolsGenerator'))
{
    /**
     * @param $type  string page|user
     * @param $id int
     * @param $params array
     * @param $help int page_help_id
     * @return $this
     */
    function shortToolsGenerator($type, $id, $params, $help = 0)
    {
        $return = getShortToolDetail($type, $id);
        // dd($type,$id,$params,$help);
        return view('hamahang.Tools.helper.short_tools_generator')
            ->with('vals', $return)
            ->with('params', $params)
            ->with('help', $help)
            ->with('type', $type)
            ->with('id', $id);
    }

}

if (!function_exists('set_item_order'))
{

    function set_item_order($model, $id, $order_step, $type, $value, $parent_id = null, $order_column_name = 'order')
    {
//        $table_name = with(new $model)->getTable();
//        $exist_parent_column = (Schema::hasColumn($table_name, $parent_id));

        $item = $model::findOrFail(deCode($id));
        if ($type == 'up')
        {
            if ($order_step == 'asc')
            {
                if ($parent_id)
                {
                    $up_item = $model::where("$order_column_name", '<', $item->$order_column_name)->where('parent_id', $item->parent_id)->orderBy("$order_column_name", 'desc')->first();
                }
                else
                {
                    $up_item = $model::where("$order_column_name", '<', $item->$order_column_name)->orderBy("$order_column_name", 'desc')->first();
                }

                if ($up_item)
                {
                    $up_item_order = $up_item->$order_column_name;
                    $up_item->$order_column_name = $item->$order_column_name;
                    $up_item->save();
                    $item->$order_column_name = $up_item_order;
                    $item->save();
                }
            }
            else
            {
                if ($parent_id)
                {
                    $up_item = $model::where("$order_column_name", '>', $item->$order_column_name)->where('parent_id', $item->parent_id)->orderBy("$order_column_name", 'desc')->first();
                }
                else
                {
                    $up_item = $model::where("$order_column_name", '>', $item->$order_column_name)->orderBy("$order_column_name", 'desc')->first();
                }

                if ($up_item)
                {
                    $up_item_order = $up_item->$order_column_name;
                    $up_item->$order_column_name = $item->$order_column_name;
                    $up_item->save();
                    $item->$order_column_name = $up_item_order;
                    $item->save();
                }
            }
        }
        elseif ($type == 'down')
        {
            if ($order_step == 'asc')
            {
                if ($parent_id)
                {
                    $up_item = $model::where("$order_column_name", '>', $item->$order_column_name)->where('parent_id', $item->parent_id)->orderBy("$order_column_name", 'asc')->first();
                }
                else
                {
                    $up_item = $model::where("$order_column_name", '>', $item->$order_column_name)->orderBy("$order_column_name", 'asc')->first();
                }

                if ($up_item)
                {
                    $up_item_order = $up_item->$order_column_name;
                    $up_item->$order_column_name = $item->$order_column_name;
                    $up_item->save();
                    $item->$order_column_name = $up_item_order;
                    $item->save();
                }
            }
            else
            {
                if ($parent_id)
                {
                    $up_item = $model::where("$order_column_name", '<', $item->$order_column_name)->where('parent_id', $item->parent_id)->orderBy("$order_column_name", 'asc')->first();
                }
                else
                {
                    $up_item = $model::where("$order_column_name", '<', $item->$order_column_name)->orderBy("$order_column_name", 'asc')->first();
                }

                if ($up_item)
                {
                    $up_item_order = $up_item->$order_column_name;
                    $up_item->$order_column_name = $item->$order_column_name;
                    $up_item->save();
                    $item->$order_column_name = $up_item_order;
                    $item->save();
                }
            }
        }
        elseif ($type == 'save')
        {
            if ($parent_id)
            {
                $old_items = $model::where('parent_id', $item->parent_id)->where("$order_column_name", '>=', $value)->get();
            }
            else
            {
                $old_items = $model::where("$order_column_name", '>=', $value)->get();
            }

            foreach ($old_items as $old_item)
            {
                $old_item->$order_column_name += 1;
                $old_item->save();
            }
            $item->$order_column_name = $value;
            $item->save();
        }
        orderItem($model, isset($item->parent_id) ? $item->parent_id : null, false, false, $order_column_name);
    }
}
if (!function_exists('orderItem'))
{
    function orderItem($model, $parent_id = null, $cat_culomn_name = false, $cat_id = false, $order_column_name = 'order')
    {
        if (isset($parent_id))
        {
            $items = $model::where('parent_id', $parent_id)->where('parent_id', $parent_id);
        }
        else
        {
            $items = $model::query();
        }
        if ($cat_culomn_name)
        {
            $items = $items->where("$cat_culomn_name", $cat_id);
        }
        $items = $items->orderBy("$order_column_name", 'asc')->get();

        $i = 0;
        foreach ($items as $item)
        {
            $item->$order_column_name = ++$i;
            $item->save();
        }
    }
}
//----------------End-------------- Menu And Tools -------------------End---------------//